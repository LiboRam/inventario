<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Area;

class Area extends Model
{
	use SoftDeletes;

	protected $table = 'areas';
	protected $fillable = ['nombreArea'];
	protected $primaryKey ='idArea';
	protected $dates = ['delete_at'];


	public function Usuarios()
	{
		return $this->hasOne('App\Usuario');
	}
}
