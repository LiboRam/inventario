<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Categoria;

class Categoria extends Model
{
	use SoftDeletes;

	protected $table = 'categorias';
	protected $fillable = ['nombreCategoria'];
	protected $primaryKey ='idCategoria';
	protected $dates = ['delete_at'];

	public function Equipo()
	{
		return $this->hasOne('App\Equipo');
	}
}
