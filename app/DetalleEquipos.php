<?php

namespace App;
use App\DetalleEquipos;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleEquipos extends Model
{
	use SoftDeletes;
	
    protected $table = 'detalle_equipos';
    protected $fillable = ['equipo_id', 'existencia'];
    protected $primaryKey = 'equipo_id';
    protected $dates = ['delete_at'];

    public function Equipo()
    {
    	return $this->belongsTo('App\Equipo','equipo_id');
    }
}
