<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DetalleSolicituds;

class DetalleSolicituds extends Model
{
    protected $table ='detalle_solicituds';
    protected $fillable = ['solicitud_id','equipo_id'];
    protected $primaryKey = 'solicitud_id';

    public function Solicitud()
    {
    	return $this->belongsTo('App\Solicitud');
    }

    public function Equipo()
    {
    	return $this->belongsTo('App\Equipo');
    }
    
}
