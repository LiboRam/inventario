<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Edoequipos;

class Edoequipos extends Model
{
	use SoftDeletes;

    protected $table = 'edoequipos';
    protected $fillable = ['nombreEdo'];
    protected $primaryKey='idEdo';
    protected $dates = ['delete_at'];
   
   public function Equipo()
    {
    	return $this->belongsTo('App\Equipo');
    }
}
