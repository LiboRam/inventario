<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Equipo;
use Carbon\Carbon;

class Equipo extends Model
{
  use SoftDeletes;

  protected $table = 'equipos';
  protected $fillable =['nombreEquipo', 'numSerie','sicipo','descripcion','fechaIngreso','foto','marca_id','modelo_id','categoria_id','unidad_id','edo_id'];
  protected $primaryKey = 'idEquipo';
  protected $dates = ['fechaIngreso', 'delete_at'];

 public function getTypeNameAttribute() // accessor role-condicion
 {
  if ($this->condicion == 'C')
    return 'Contable';
  if ($this->condicion == 'N')
    return 'No contable';
}

public function Marca()
{
 return $this->belongsTo('App\Marca', 'marca_id');
}

public function Modelo()
{
 return $this->belongsTo('App\Modelo', 'modelo_id');
}

public function Categoria()
{
 return $this->belongsTo('App\Categoria', 'categoria_id');
}

public function Unidad()
{
 return $this->belongsTo('App\Unidad','unidad_id');
}

public function EdoEquipos()
{
 return $this->belongsTo('App\EdoEquipos','edo_id');
}

public function DetalleEquipos()
{
 return $this->hasOne('App\DetalleEquipos','equipo_id');
} 
 

public function Resguardo()
{
  return $this->belongsToMany('App\Resguardo');
} 

public function Solicitud()
    {
      return $this->belongsToMany('App\Solicitud');
    }

/* Subir una sola imagen*/
public function setPathAttribute($path){
  $this->attributes['path'] = Carbon::now()->second.$path->getClientOriginalName();
  $name = Carbon::now()->second.$path->getClientOriginalName();
  \Storage::disk('local')->put($name, \File::get($path));
}

/*public static function createEq($datos)
{
  DB::transaction(function () use ($datos){
    $equipo = Equipo::create([
     'nombreEquipo'=> $datos['nombreEquipo'],
     'numSerie'=> $datos['numSerie'],
     'sicipo'=> $datos['sicipo'],
     'descripcion'=> $datos['descripcion'],
     'fechaIngreso'=> $datos['fechaIngreso'],
     'foto'=> $datos->file('foto')->store['foto'],
     'marca_id'=> $datos['marca_id'],
     'modelo_id'=> $datos['modelo_id'],
     'categoria_id'=> $datos['categoria_id'],
     'unidad_id'=> $datos['unidad_id'],
     'edo_id'=> $datos['edo_id']

    ]);

    $equipo->detalleEquipos()->create([
     'existencia'->$datos['existencia'],

    ]);
  });

} */

}

