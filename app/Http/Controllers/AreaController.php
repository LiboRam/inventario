<?php

namespace App\Http\Controllers;
use Session;
use App\Area;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class AreaController extends Controller
{
    public function index()
    {
        $areas = Area::orderBy('idArea')->get();
        return view('areas.index',['areas' => $areas]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('areas.create');
    }

public function search(Request $request)
{
 $searchA = $request->get('searchA');

 $areas =DB::table('areas')->where('nombreArea', 'ilike', '%'.$searchA.'%')->paginate(8);
 return view('areas.index', ['areas'=>$areas]);

}
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = $request->all();
        Area::create($datos);

        Session::flash('message', $datos['nombreArea'] . ' Agregado exitosamente!');
        return redirect('/areas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idArea)

    {
        $area =Area::find($idArea);
        return view('areas/edit', ['area' => $area]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idArea)
    {
        $area = Area::find($idArea);
        $datos = $request->all();
        $area->update($datos);

        Session::flash('message', $area['nombreArea'] . ' Actualizado exitosamente!');
        return redirect('/areas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  public function destroy($idArea)
   {
        $area = Area::find($idArea);
        $area->destroy($idArea);

       Session::flash('message',$area['nombreArea'] . ' Eliminado correctamente!');
        return redirect('/areas');
    } 
}
