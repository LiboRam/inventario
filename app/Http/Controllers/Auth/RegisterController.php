<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Area;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facedes\input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    public function index()
    {
        /*$usuario = Usuario::orderBY('idUsuario')->get();
        return view('usuarios.index', ['usuarios'=> $usuario]);*/
        $user = User::all();
       return view('usuarios.index', compact('usuario'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|string|max:55',
            'password' => 'required|string|min:6|max:32|confirmed',
            'nombreUsuario' => 'required|string|max:35',
            'apellidoP' => 'required|string|max:35',
            'apellidoM' => 'required|string|max:35',
            'puesto' => 'required|string|max:150',
            'fechaCreacion' => 'required',
            'area_id' => 'required|exists:areas, idArea',
            
        ]);
    }

    protected function create(array $data)
    {
        return User::create([
            'username' => $data['username'],
            'password' => bcrypt($data['password']),
            'nombreUsuario' => $data['nombreUsuario'],
            'apellidoP' => $data['apellidoP'],
            'apellidoM' => $data['apellidoM'],
            'puesto' => $data['puesto'],
            'fechaCreacion' => $data['fechaCreacion'],
            'area_id' => $data['area_id'],

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

    public function store(Request $request)
    {
        $user = $request->all();
        User::create($user);

        Session::flash('message', $user['nombreUsuario'] . ' agregado exitosamente');
        return redirect('/usuarios');     
    }
}
