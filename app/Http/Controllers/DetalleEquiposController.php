<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use App\DetalleEquipos;
use App\Equipo;

class DetalleEquiposController extends Controller
{
   public function index()
   {
    $detalle = DetalleEquipos::orderBy('equipo_id')->get();
    return view('detalle_equipos.index', ['detalle_equipos' => $detalle]);
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$equipo = Equipo::all();
        return view('detalle_equipos.create', compact('equipo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = $request->all();
        DetalleEquipos::create($datos);
        return redirect('/detalle_equipos')->with('message', 'Equipo insertado exitosamente !'); 
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($equipo_id)

    {
    	$equipo = Equipo::all();
        $detalle_equipos = DetalleEquipos::find($equipo_id);
        return view('detalle_equipos/edit', ['detalle_equipos' => $detalle_equipos], compact('equipo'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $equipo_id)
    {
        $detalle_equipos = DetalleEquipos::find($equipo_id);
        $datos = $request->all();
        $detalle_equipos->update($datos);

        Session::flash('message', $detalle_equipos['equipo_id'] . ' Actualizado exitosamente!');
        return redirect('/detalle_equipos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($equipo_id)
    {
        $detalle_equipos = DetalleEquipos::find($equipo_id);
        $detalle_equipos->destroy($equipo_id);

        Session::flash('message', $detalle_equipos['equipo_id'] . ' Eliminado correctamente!');
        return redirect('/detalle_equipos');
    }

}
