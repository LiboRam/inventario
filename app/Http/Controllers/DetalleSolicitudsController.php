<?php

namespace App\Http\Controllers;
use Session;
use App\Solicitud;
use App\Equipo;
use App\DetalleSolicituds;

use Illuminate\Http\Request;

class DetalleSolicitudsController extends Controller
{
    public function index()
    {
        $detalleSolicitud = DetalleSolicituds::orderBy('solicitud_id')->get();
        return view('detalle_solicituds.index', ['detalle_solicituds' => $detalleSolicitud]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$solicitud = Solicitud::all();
    	$equipo = Equipo::all();
        return view('detalle_solicituds.create', compact('solicitud', 'equipo'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = $request->all();
        DetalleSolicituds::create($datos);

        Session::flash('message', $datos['solicitud_id'] . ' Agregado exitosamente!');
        return redirect('/detalle_solicituds');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($solicitud_id)
    {
    	$solicitud = Solicitud::all();
    	$equipo = Equipo::all();
        $detalleSolicitud = DetalleSolicituds::find($solicitud_id);
        return view('detalle_solicituds/edit', ['detalle_solicituds' => $detalleSolicitud], compact('solicitud','equipo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $solicitud_id)
    {
        $detalleSolicitud = DetalleSolicituds::find($solicitud_id);
        $datos = $request->all();
        $detalleSolicitud->update($datos);

        Session::flash('message', $detalleSolicitud['solicitud_id'] . ' Actualizado exitosamente!');
        return redirect('/detalle_solicituds');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($solicitud_id)
    {
        $detalleSolicitud = DetalleSolicituds::find($solicitud_id);
        $detalleSolicitud->destroy($solicitud_id);

        Session::flash('message', $detalleSolicitud['solicitud_id'] . ' Eliminado correctamente!');
        return redirect('/detalle_solicituds');
    }
}