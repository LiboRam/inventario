<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use\App\EdoEquipos;

class EdoEquiposController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $edoequipo = Edoequipos::orderBy('idEdo')->get();
        return view('edoequipos.index',['edoequipos'=> $edoequipo]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('edoequipos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = $request->all();
        Edoequipos::create($datos);

        Session::flash('message', $datos['nombreEdo'] . ' Agregado Exitosamente');
        return redirect('/edoequipos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idEdo)
    {
        $edoequipos = Edoequipos::find($idEdo);
        return view('edoequipos/edit', ['edoequipos' => $edoequipos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idEdo)
    {
        $edoequipos = Edoequipos::find($idEdo);
        $datos = $request->all();
        $edoequipos->update($datos);

        Session::flash('message', $edoequipos['nombreEdo'] . ' Actualizado Exitosamente!');
        return redirect('/edoequipos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idEdo)
    {
        $edoequipos = Edoequipos::find($idEdo);
        $edoequipos->destroy($idEdo);

        Session::flash('message', $edoequipos['nombreEdo'] . '  Eliminado correctamente!');
        return redirect('/edoequipos');
    }
}