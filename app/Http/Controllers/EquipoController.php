<?php

namespace App\Http\Controllers;
use Session;
use Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Storage;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\Equipo;
use App\Marca;
use App\Modelo;
use App\Categoria;
use App\Unidad;
use App\EdoEquipos;
use PDF;
use BD;

class EquipoController extends Controller
{
    /**
     * Display a listing of the resource.
     *

     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     $equipo = Equipo::all();
     return view('equipos.index', compact('equipo'));
 }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   $equipo = Equipo::all();
    	$marca = Marca::all();
    	$modelo = Modelo::all();
    	$categoria = Categoria::all();
    	$unidad = Unidad::all();
    	$edoequipo = EdoEquipos::all();
        return view('equipos.create', compact('equipo','marca', 'modelo', 'categoria', 'unidad', 'edoequipo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {  

        $equipo = new Equipo;
        $equipo->nombreEquipo=$request->nombreEquipo;
       // $equipo->condicion=$request->condicion;
        $equipo->numSerie = $request->numSerie;
        $equipo->sicipo = $request->sicipo;
        $equipo->descripcion = $request->descripcion;
        $equipo->fechaIngreso = $request->fechaIngreso;
        $equipo->foto = $request->file('foto')->store('equipos');
        $equipo->marca_id = $request->marca_id;
        $equipo->modelo_id = $request->modelo_id;
        $equipo->categoria_id = $request->categoria_id;
        $equipo->unidad_id = $request->unidad_id;
        $equipo->edo_id = $request->edo_id;
        
        $equipo->save();


        return redirect('/equipos')->with('message', ' Equipo insertado exitosamente !'); 
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function generate(Request $request){

        $equipo = Equipo::all();
        $pdf = PDF::loadView('equipos.informe',['equipos'=>$equipo]);
        PDF::loadHTML($pdf)->setPaper('a4', 'landscape')->setWarnings(false)->save('informe.pdf');
        return $pdf->download('informe equipos.pdf');

    }


    public function show($idEquipo)
    {
        $equipo = Equipo::all();
        $today = Carbon::now()->format('d/m/Y');
        $pdf = PDF::loadView('equipos.informe',['equipos'=>$equipo], compact('today'));
        return $pdf->download('informe equipos.pdf');

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idEquipo)
    {
    	$marca = Marca::all();
    	$modelo = Modelo::all();
    	$categoria = Categoria::all();
    	$unidad = Unidad::all();
    	$edoequipo = EdoEquipos::all();
        $equipo = Equipo::find($idEquipo);
        return view('equipos/edit', ['equipos' => $equipo], compact('equipo','marca', 'modelo', 'categoria', 'unidad', 'edoequipo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idEquipo)
    {
        $equipo = Equipo::find($idEquipo);
        $equipo->nombreEquipo=$request->nombreEquipo;
      //  $equipo->condicion=$request->condicion;
        $equipo->numSerie = $request->numSerie;
        $equipo->sicipo = $request->sicipo;
        $equipo->descripcion = $request->descripcion;
        $equipo->fechaIngreso = $request->fechaIngreso;
        if($request->hasFile('foto')) {
            $equipo->foto = $request->file('foto')->store('equipos'); 
        }

        $equipo->marca_id = $request->marca_id;
        $equipo->modelo_id = $request->modelo_id;
        $equipo->categoria_id = $request->categoria_id;
        $equipo->unidad_id = $request->unidad_id;
        $equipo->edo_id = $request->edo_id;
        $equipo->save();

        Session::flash('message', 'Equipo actualizado satisfactoriamente !');
        return Redirect::to('/equipos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idEquipo)
    {
        $equipo = Equipo::find($idEquipo);

        foreach ($equipo as $image) {
         Storage::delete($image['foto']);
     }

     $equipo->destroy($idEquipo);

     Session::flash('message', ' Equipo eliminado correctamente!');
     return redirect('/equipos');
 }

 public function stock()
 {
    $equipos = Equipo::all();
    return view('equipos.stock')->with(compact('equipos'));
}

public function informe()
{
    $equipos = Equipo::with(['Ubicacion', 'Solicitante'])->get();
   //return view('equipos.filtro')->with(compact('equipos','ubicacions','solicitantes'));
    return response()->json(
     $equipos 
 ); 


}


}
