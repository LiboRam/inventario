<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Marca;

class MarcaController extends Controller
{
    public function index()
    {
        $marca = Marca::orderBy('idMarca')->get();
        return view('marcas.index',['marcas' => $marca]);
    }

    public function create()
    {
        return view('marcas.create');
    }

    public function search(Request $request)
{
 $searchM = $request->get('searchM');

 $marcas =DB::table('marcas')->where('nombreMarca', 'ilike', '%'.$searchM.'%')->paginate(8);
 return view('marcas.index', ['marcas'=>$marcas]);

}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dato = $request->all();
        Marca::create($dato);

        Session::flash('message', $dato['nombreMarca'] . ' Agregado exitosamente!');
        return redirect('/marcas');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($idMarca)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idMarca)
    {
        $marca = Marca::find($idMarca);
        return view('marcas/edit', ['marca' => $marca]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idMarca)
    {
        $marca = Marca::find($idMarca);
        $dato = $request->all();
        $marca->update($dato);

        Session::flash('message', $marca['nombreMarca'] . ' Actualizado exitosamente!');
        return redirect('/marcas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idMarca)
    {
        //
        $marca = Marca::find($idMarca);
        $marca->destroy($idMarca);

        Session::flash('message', $marca['nombreMarca'] . ' Eliminado exitosamente!');
        return redirect('/marcas');
    }
}