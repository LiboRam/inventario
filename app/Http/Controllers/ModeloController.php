<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Modelo;

class ModeloController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modelo = Modelo::orderBy('idModelo')->get();
        return view('modelos.index',['modelos' => $modelo]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modelos.create');
    }



    public function search(Request $request)
{
 $searchml = $request->get('searchml');

 $modelos =DB::table('modelos')->where('nombreModelo', 'ilike', '%'.$searchml.'%')->paginate(8);
 return view('modelos.index', ['modelos'=>$modelos]);

}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = $request->all();
        Modelo::create($datos);

        Session::flash('message', $datos['nombreModelo'] . ' Agregado exitosamente!');
        return redirect('/modelos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($idModelo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idModelo)
    {
        $modelo = Modelo::find($idModelo);
        return view('modelos/edit',['modelo' => $modelo]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idModelo)
    {
        $modelo = Modelo::find($idModelo);
        $datos = $request->all();
        $modelo->update($datos);

        Session::flash('message', $modelo['nombreModelo'] .  ' Actualizado exitosamente!');
        return redirect('/modelos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idModelo)
    {
        $modelo = Modelo::find($idModelo);
        $modelo->destroy($idModelo);

        Session::flash('message', $modelo['nombreModelo'] .  ' Eliminado exitosamente!');
        return redirect('/modelos');
    }
}