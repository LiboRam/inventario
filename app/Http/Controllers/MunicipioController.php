<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Request\MunicipioRequest;
use App\Municipio;

class MunicipioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $municipios = Municipio::orderBy('idMunicipio')->get();
        return view('municipios.index',['municipios' => $municipios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('municipios.create');
    }



public function search(Request $request)
{
 $searchm = $request->get('searchm');

 $municipios =DB::table('municipios')->where('nombreMunicipio', 'ilike', '%'.$searchm.'%')->paginate(8);
 return view('municipios.index', ['municipios'=>$municipios]);

}
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validarDato = $request->validate([
        'clave' => 'required|max: 3',
         'nombreMunicipio' => 'required',
          'region' => 'required',
      ]);  

        $datos = $request->all();
        Municipio::create($datos);

        Session::flash('message', $datos['nombreMunicipio'] . ' Agregado exitosamente');
        return redirect('/municipios');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idMunicipio)
    {
       $municipio = Municipio::find($idMunicipio);
       return view('municipios/edit', ['municipio' => $municipio]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idMunicipio)
    {
        $municipio = Municipio::find($idMunicipio);
        $datos = $request->all();
        $municipio->update($datos);

        Session::flash('message', $municipio['nombreMunicipio'] . ' Actualizado exitosamente!');
        return redirect('/municipios');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy ($idMunicipio)
    {
        $municipio = Municipio::find($idMunicipio);
        $municipio->destroy($idMunicipio);

        Session::flash('message', $municipio['nombreMunicipio'] . ' Eliminado correctamente!');
        return redirect('/municipios');
    }
}