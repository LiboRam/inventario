<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\getView;
use App\Equipo;
use App\Resguardo;
use App\Solicitud;
use App\Ubicacion;
use App\Solicitante;
use PDF;
use BD;

class ResguardoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resguardos = Resguardo::orderBY('idResguardo')->get();
        return view('resguardos.index', ['resguardos'=> $resguardos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $equipo = Equipo::all();
        $solicitud = Solicitud::all();
        $ubicacion = Ubicacion::all();
        return view('resguardos.create', compact('solicitud', 'ubicacion','equipo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = $request->all();
        Resguardo::create($datos);

        Session::flash('message', $datos['nombreResguardo'] . ' Agregado exitosamente');
        return redirect('/resguardos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idResguardo)
    {
        $equipo = Equipo::all();
        $solicitud = Solicitud::all();
        $ubicacion = Ubicacion::all();
        $resguardo = Resguardo::find($idResguardo);
        return view('resguardos/edit', ['resguardo' => $resguardo], compact('solicitud', 'ubicacion','equipo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idResguardo)
    {
        $resguardo = Resguardo::find($idResguardo);
        $datos = $request->all();
        $resguardo->update($datos);

        Session::flash('message', $resguardo['nombreResguardos'] .  ' Actualizado exitosamente');
        return redirect('/resguardos');

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idResguardo)
    {
        $resguardo = Resguardo::find($idResguardo);
        $resguardo->destroy($idResguardo);

        Session::flash('message', $resguardo['nombreResguardos'] . ' Eliminado correctamente');
        return redirect('/resguardos');
    }

    public function informe()
    {
        $resultados = Resguardo::with(['Solicitante','Solicitud','Ubicacion'])->get();
        //$today = Carbon::now()->format('d/m/Y');
        $pdf = PDF::loadView('resguardos.informe',['resultados'=> $resultados], compact('today'));
        return $pdf->download('formato resguardo.pdf'); 

       /* return response()->json(
           $resultados 
       ); */
    }

}
