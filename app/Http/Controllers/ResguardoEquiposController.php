<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use App\ResguardoEquipos;
use App\Resguardo;

class ResguardoEquiposController extends Controller
{
    public function index()
    {
        $res = ResguardoEquipos::orderBy('resguardo_id')->get();
        return view('resguardo_equipos.index', ['resguardo_equipos' => $res]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$resguardos = Resguardo::all();
        return view('resguardo_equipos.create', compact('resguardos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = $request->all();
        ResguardoEquipos::create($datos);

        Session::flash('message', $datos['resguardo_id'] . ' Agregado exitosamente!');
        return redirect('/resguardo_equipos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)

    {
    	$resguardos = Resguardo::all();
        $resguardo_equipos = ResguardoEquipos::find($resguardo_id);
        return view('resguardo_equipos/edit', ['resguardo_equipos' => $resguardo_equipos], compact('resguardos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $resguardo_equipos = ResguardoEquipos::find($resguardo_id);
        $datos = $request->all();
        $resguardo_equipos->update($datos);

        Session::flash('message', $resguardo_equipos['resguardo_id'] . ' Actualizado exitosamente!');
        return redirect('/resguardo_equipos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $resguardo_equipos = ResguardoEquipos::find($resguardo_id);
        $resguardo_equipos->destroy($resguardo_id);

        Session::flash('message', $resguardo_equipos['resguardo_id'] . ' Eliminado correctamente!');
        return redirect('/resguardo_equipos');
    }



}