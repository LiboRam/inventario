<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use Illuminate\Http\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use App\Http\Request\StoreSolicitanteRequest;
use Carbon\Carbon;
use App\Solicitante;
use PDF;
use BD;

class SolicitanteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $solicitantes = Solicitante::orderBY('idSolicitante')->get();
        return view('solicitantes.index', ['solicitantes'=> $solicitantes]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('solicitantes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request  $request)
    { 
        $datos = $request->all();
        Solicitante::create($datos);

        Session::flash('message', $datos['nombreSolicitante'] . ' Agregado exitosamente');
        return redirect('/solicitantes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request  $request){
    //Obtener todos los solicitantes de la BD
      $solicitante = Solicitante::all();
    //imprime la fecha del impresion del pdf
      $today = Carbon::now()->format('d/m/Y');
    //Enviar datos a la vista usando la funcion loadVista de la fachada PDF
      $pdf = PDF::loadView('solicitantes.reporte', ['solicitantes'=> $solicitante], compact('today'));
    //para descargar el archivo se usa la sig. función
      return $pdf->download('solicitantes.pdf');
  }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idSolicitante)
    {
        $solicitante = Solicitante::find($idSolicitante);
        return view('solicitantes/edit', ['solicitante' => $solicitante]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idSolicitante)
    {
        $solicitante = Solicitante::find($idSolicitante);
        $datos = $request->all();
        $solicitante->update($datos);

        Session::flash('message', $solicitante['nombreSolicitante'] .  ' Actualizado exitosamente');
        return redirect('/solicitantes');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idSolicitante)
    {
        $solicitante = Solicitante::find($idSolicitante);
        $solicitante->destroy($idSolicitante);

        Session::flash('message', $solicitante['nombreSolicitante'] . ' Eliminado correctamente');
        return redirect('/solicitantes');
    }

}
