<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use Illuminate\Http\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use App\Http\Request\StoreSolicitudRequest;
use Carbon\Carbon;
use App\Equipo;
use App\Solicitud;
use App\Solicitante;
use App\Usuario;
use PDF;
use BD;

class SolicitudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$solicitud = Solicitud::orderBy('idSolicitud')->get();
        //return view('solicituds.index', ['solicituds' => $solicitud]);
        $solicituds = Solicitud::all();
        return view ('solicituds.index', compact('solicituds'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {    
        $equipo = Equipo::all();
        $solicitud = Solicitud::all();
        $solicitantes = Solicitante::all();
        $usuarios = Usuario::all();
        return view('solicituds.create', compact('solicitantes','equipo','usuarios','solicitud'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = $request->all();
        Solicitud::create($datos);
        
        Session::flash('message', $datos['nombreSolicitud'] . ' Agregado exitosamente');
        return redirect('/solicituds');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idSolicitud)
    {
        $equipo = Equipo::all();
        $solicitante = Solicitante::all();
        $usuario = Usuario::all();
        $solicitud = Solicitud::find($idSolicitud);
        return view('solicituds/edit', ['solicitud' => $solicitud], compact('equipo','solicitante', 'usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idSolicitud)
    {
        $solicitud = Solicitud::find($idSolicitud);
        $datos = $request->all();
        $solicitud->update($datos);

        Session::flash('message', $solicitud['nombreSolicitud'] .  ' Actualizado exitosamente');
        return redirect('/solicituds');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idSolicitud)
    {
        $solicitud = Solicitud::find($idSolicitud);
        $solicitud->destroy($idSolicitud);

        Session::flash('message', $solicitud['nombreSolicitud'] .  ' Eliminado correctamente');
        return redirect('/solicituds');
    }

    public function show( Request $idSolicitud)
    {
        $solicituds = Solicitud::with('Equipo','Solicitante')
        ->find($idSolicitud);
        $today = Carbon::now()->format('d/m/Y');
        $pdf = PDF::loadView('solicituds.generate',['solicitud'=> $solicituds], compact('today'));
        return $pdf->download('formato solicitud.pdf'); 
       /* return response()->json(
         $solicituds 
     ); */
 } 
}


/*
 $product = App\Product::with('options')->with('options.values')->first(); foreach ($product->options as $option) { foreach ($option->values()->where('product_id', $product->id)->get() as $value) { printf("PRO: %s, OPT: %d, VAL: %d
\n", $product->id, $option->id, $value->id); } } 


 $values = ProductOptionValue::select([ 'product_option_values.*', 'product_option_to_product.option_id', ]) ->join('product_option_to_product', 'product_option_to_product.product_option_value_id', '=', 'product_option_values.id') ->where('product_id', $product->id) ->get();

*/