<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use App\Ubicacion;
use App\Municipio;

class UbicacionController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ubicacions = Ubicacion::orderBy('idUbicacion')->get();
        return view('ubicacions.index',['ubicacions' => $ubicacions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $municipios = Municipio::all();
        return view('ubicacions.create', compact('municipios'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = $request->all();
        Ubicacion::create($datos);

        Session::flash('message', $datos['nombreUbicacion'] . ' agregado exitosamente');
        return redirect('/ubicacions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idUbicacion)
    {
        $municipios = Municipio::all();
        $ubicacions = Ubicacion::find($idUbicacion);
        return view('ubicacions/edit',['ubicacions' => $ubicacions], compact('municipios'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idUbicacion)
    {
        $ubicacions = Ubicacion::find($idUbicacion);
        $datos = $request->all();
        $ubicacions->update($datos);

        Session::flash('message', $ubicacions['nombreUbicacion'] .  ' update successfully');
        return redirect('/ubicacions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idUbicacion)
    {
        $ubicacions = Ubicacion::find($idUbicacion);
        $ubicacions->destroy($idUbicacion);

        Session::flash('message', $ubicacions['nombreUbicacion'] .  ' eliminado correctamente');
        return redirect('/ubicacions');
    }
}