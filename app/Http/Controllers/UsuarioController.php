<?php

namespace App\Http\Controllers;
use Session;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facedes\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Area;

class UsuarioController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$usuario = Usuario::orderBY('idUsuario')->get();
        return view('usuarios.index', ['usuarios'=> $usuario]);*/
        $usuario = User::all();
       return view('usuarios.index', compact('usuario'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        
    	$area = Area::all();
        return view('auth.register', compact('area'));
        
    }


public function search(Request $request)
{
 $searchU = $request->get('searchU');

 $municipios =DB::table('usuarios')->where('nombreUsuario', 'ilike', '%'.$searchU.'%')->paginate(8);
 return view('usuarios.index', ['usuarios'=>$usuarios]);

}
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $usuario = $request->all();
       User::create($usuario);


/*
        $validator = Validator::make($request->all(),
        [
            'usuario' => 'required|max:55',
            'password' => 'required|between:6,12|confirmed|regex:/^[A-Za-z0-9@!#\$%\^&\*]+$/',
            'nombreUsuario' => 'required|max:35',
            'apellidoP' => 'required|max:35',
            'apellidoM' => 'required|max:35',
            'puesto' => 'required|max:150',
            'fechaCreacion' => 'required',
            'area_id' => 'required'
        ]);

        if ($validator->fails())
        {
            return redirect('usuarios/create')
                ->withErrors($validator)
                ->withInput();
        }
        
        
*/
        Session::flash('message', $usuario['nombreUsuario'] . ' agregado exitosamente');
        return redirect('/usuarios');     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($idUsuario)
    {
        $area = Area::all();
        $usuario = User::find($idUsuario);
        return view('usuarios/edit', ['usuario' => $usuario], compact('area'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $idUsuario)
    {
        $usuario = User::find($idUsuario);
        $datos = $request->all();
        $usuario->update($datos);

        Session::flash('message', $usuario['usuario'] .  ' Actualizado exitosamente');
        return redirect('/usuarios');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = User::find($idUsuario);
        $usuario->destroy($idUsuario);

        Session::flash('message', $usuario['usuario'] . ' Eliminado correctamente');
        return redirect('/usuarios');
    }

}

