<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSolicitanteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
         $comment = Comment::find($this->route('comment'));

    return $comment && $this->user()->can('update', $comment)
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombreSolicitante' => 'required',
            'apellidoP' => 'required',
            'apellidoM' => 'required',
            'cargo' => 'required',
            'dependencia' => 'required',
            'telefono' => 'required|numerico|max:15',
        ];
    }
}
