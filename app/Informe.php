<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\Equipo;
use App\Solicitante;
use App\Ubicacion;
use App\Ubicacion;
use App\Solicitante;

class Informe extends Model
{
    //
	public function Equipo()
	{
		return $this->hasMany('App\Equipo', 'equipo_id');
	}
    //
	public function Solicitante()
	{
		return $this->belongsTo('App\Solicitante', 'solicitante_id');
	}
    //
	public function Ubicacion()
	{
		return $this->belongsTo('App\Ubicacion', 'ubicacion_id');
	}
    

}
