<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Marca;

class Marca extends Model
{
	use SoftDeletes;

   protected $table = 'marcas';
   protected $fillable = ['nombreMarca'];
   protected $primaryKey = 'idMarca';
   protected $dates = ['delete_at'];

   public function Equipo()
    {
    	return $this->hasOne('App\Equipo');
    }
}
