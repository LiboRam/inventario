<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Modelo;

class Modelo extends Model
{
	use SoftDeletes;

    protected $table = 'modelos';
    protected $fillable = ['nombreModelo'];
    protected $primaryKey= 'idModelo';
    protected $dates = ['delete_at'];

    public function Equipo()
    {
    	return $this->hasOne('App\Equipo');
    }
}
