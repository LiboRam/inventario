<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Municipio;

class Municipio extends Model
{
	use SoftDeletes;
    protected $table = 'municipios';
    protected $fillable = ['clave', 'nombreMunicipio', 'region'];
    protected $primaryKey = 'idMunicipio';
    protected $dates = ['delete_at'];

    public function Ubicacion()
    {
    	return $this->hasOne('App\Ubicacion');
    }
}
