<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Resguardo;

class Resguardo extends Model
{
    use SoftDeletes;
    
    protected $table = 'resguardos';
    protected $fillable = ['nombreResguardo', 'fechaInicio', 'fechaFin', 'observaciones','solicitud_id', 'ubicacion_id'];
    protected $primaryKey = 'idResguardo';
    protected $dates = ['fechaInicio', 'fechaFin', 'delete_at'];

    public function Solicitud()
    {
    	return $this->belongsTo('App\Solicitud', 'solicitud_id');
    }  

    public function Ubicacion()
    {
    	return $this->belongsTo('App\Ubicacion', 'ubicacion_id');
    }

    public function ResguardoEquipos()
    {
    	return $this->hasOne('App\ResguardoEquipos');
    }

   /* public function Equipo()
    {
        return $this->hasOne('App\Equipo');
    } */
    
    
    public function Equipo()
    {
        return $this->belongsTo('App\Equipo','equipo_id');
    } 

    public function Solicitante()
    {
        return $this->hasOne('App\Solicitante','idSolicitante');
    }
   /* $resguardos = Resguardo::query()->select(
            'resguardo.id',
            'resguardo.nombreResguardo',
            'resguardo.fechaInicio',
            'resguardo.fechaFin',
            'resguardo.observaciones',
            'resguardo.solicitud_id',
            'resguardo.ubicacion_id',
        )->with(
            ['rela_solicitud' => function( $query ) {
                $query->select('id','nombreSolicitud');
            }]
       
        )->with(
            ['rela_ubicacion' => function( $query ) {
                $query->select('id', 'nombreUbicacion');
            }]
        ); */
    }
