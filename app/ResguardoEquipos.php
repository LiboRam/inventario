<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\ResguardoEquipos;

class ResguardoEquipos extends Model
{
	use SoftDeletes;
	
    protected $table = 'resguardo_equipos';
    protected $fillable = ['cantidadEntregado'];
    protected $dates = ['delete_at'];

    public function Resguardo()
    {
    	return $this->belongsTo('App\Resguardo');
    }
}

