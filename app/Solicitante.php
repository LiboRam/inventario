<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Solicitante;

class Solicitante extends Model
{
	use SoftDeletes;
	
    protected $table = 'solicitantes';
    protected $fillable = ['nombreSolicitante', 'apellidoP', 'apellidoM', 'cargo', 'dependencia', 'telefono'];
    protected $primaryKey = 'idSolicitante';
    protected $dates = ['delete_at'];

    public function Solicitud()
    {
    	return $this->hasMany('App\Solicitud');
    }

     public function getNameCompleteAttribute() // accessor nombreSolicitante
     {
        return $this->nombreSolicitante.' '.$this->apellidoP.' '.$this->apellidoM; // ?
    }
    


     public function Resguardo()
    {
        return $this->hasMany('App\Resguardo','idResguardo');
    }
   
}
