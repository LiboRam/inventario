<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Solicitud;

class Solicitud extends Model
{
    use SoftDeletes;
    
    protected $table = 'solicituds';
    protected $fillable = ['nombreSolicitud', 'descripcion', 'fechaSolicitud', 'fechaEntrega', 'status' ,'solicitante_id','equipo_id','usuario_id'];
    protected $primaryKey = 'idSolicitud';
    protected $dates = ['fechaSolicitud', 'fechaEntrega', 'delete_at'];

//
   /* public function Equipo()
    {
        return $this->hasMany('App\Equipo','equipo_id');
    } 
*/

    public function marca(){
        return $this->equipos()->with('marca');
    }
    
     public function Equipo()
    {
        return $this->belongsTo('App\Equipo','equipo_id');
    } 
    
    public function Solicitante()
    {
    	return $this->belongsTo('App\Solicitante','solicitante_id');
    }
    
    public function Usuario()
    {
    	return $this->belongsTo('App\Usuario', 'usuario_id');
    }

    public function Resguardo()
    {
    	return $this->hasOne('App\Resguardo');
    } 

    


  /* $solicitud = Solicitud::query()->select(
    'Solicituds.idSolicitud',
    'solicituds.nombreSolicitud',
    'solicituds.descripcion',
    'solicituds.fechaSolicitud',
    'solicituds.fechaEntrega',
    'solicituds.status',
    'solicituds.equipo_id',
    'solicituds.solicitante_id',
    'solicituds.usuario_id'
    )->with(
    ['cat_equipo' => function($query ){
        $query->select('idEquipo','nombreEquipo');
        }]
        )->with(
        ['cat_solicitante'=> function($query){
            $query->select('idSolicitante',\DB::raw("CONCAT(nombreSolicitante, '', IFNULL(apellidoP, '', IFNULL(apellidoM, '')) AS nombreSolicitante"));
            }])
        )->with(
        ['cat_usuario' => function($query){
            $query->select('idUsuario','nombreUsuario');
            }]
        );  */

    }
