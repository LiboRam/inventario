<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Ubicacion;

class Ubicacion extends Model
{
	use SoftDeletes;
	
    protected $table = 'ubicacions';
    protected $fillable = ['nombreUbicacion', 'calle', 'numInterior', 'numExterior', 'colonia', 'lat', 'lng', 'municipio_id'];
    protected $primaryKey = 'idUbicacion';
    protected $dates = ['delete_at'];


    public function Municipio()
    {
    	return $this->belongsTo('App\Municipio', 'municipio_id');
    }

   
   public function Equipo()
    {
        return $this->hasMany('App\Equipo');
    }


}
