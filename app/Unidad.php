<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Unidad;

class Unidad extends Model
{
   protected $table = 'unidads';
   protected $fillable = ['nombreUnidad'];
   protected $primaryKey = 'idUnidad';
   protected $dates = ['delete_at'];

   public function Equipo()
    {
    	return $this->hasOne('App\Equipo');
    }
}
