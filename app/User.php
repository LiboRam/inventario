<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use App\User;

class User extends Authenticatable
{
    use HasRoles;
    use Notifiable;
    use SoftDeletes;

    protected $table = 'usuarios';
    protected $fillable = ['username', 'password', 'nombreUsuario', 'apellidoP', 'apellidoM', 'puesto', 'fechaCreacion', 'area_id'];
    protected $hidden = [
        'password', 'remember_token'];
    protected $primaryKey = 'idUsuario';
    protected $dates = ['fechaCreacion', 'delete_at'];

    public function Area()
    {
        return $this->belongsTo('App\Area', 'area_id');
    }

    public function getNameCompleteAttribute() // accessor nombreUsuario
    {
        return $this->nombreUsuario.' '.$this->apellidoP.' '.$this->apellidoM; // ?
    }


}
