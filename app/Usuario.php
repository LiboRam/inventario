<?php
namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\Authenticatable as AuthenticatableContract;
use App\Usuario;

class Usuario extends Model
{
    use Notifiable;
	use SoftDeletes;

    protected $table = 'usuarios';
    protected $fillable = ['usuario', 'password', 'nombreUsuario', 'apellidoP', 'apellidoM', 'puesto', 'fechaCreacion', 'area_id', 'role_id'];
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $primaryKey = 'idUsuario';
    protected $dates = ['fechaCreacion', 'delete_at'];

    public function Area()
    {
    	return $this->belongsTo('App\Area', 'area_id');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'role_id')
        ->withTimestamps();
    }

     public function getNameCompleteAttribute() // accessor nombreUsuario
     {
        return $this->nombreUsuario.' '.$this->apellidoP.' '.$this->apellidoM; // ?
    }

    public function authorizeRoles($roles)
    {
        if ($this->hasAnyRole($roles)) {
            return true;
        }
        abort(401, 'Esta acción no está autorizada.');
    }

    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }

    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }

}
