<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->bigIncrements('idUsuario')->unique();
            $table->string('username',55)->unique();                      
            $table->string('password',32)->unique();
            $table->string('nombreUsuario',35);
            $table->string('apellidoP',35);
            $table->string('apellidoM',35);
            $table->string('puesto',150);;                                               
            $table->date('fechaCreacion')->notNullable();
            $table->unsignedInteger('area_id');
            $table->foreign('area_id')->references('idArea')->on('areas')->onDelete('cascade');
            $table->softDeletes();
            $table->rememberToken();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
