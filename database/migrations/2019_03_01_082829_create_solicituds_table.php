<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicituds', function (Blueprint $table) {
            $table->bigIncrements('idSolicitud')->unique();
            $table->string('nombreSolicitud',255);
            $table->text('descripcion');
            $table->date('fechaSolicitud')->notNullable();
            $table->date('fechaEntrega')->notNullable();
            $table->string('status',25);
        
            $table->unsignedInteger('solicitante_id');
            $table->foreign('solicitante_id')->references('idSolicitante')->on('solicitantes')->onDelete('cascade');

            $table->unsignedInteger('equipo_id');
            $table->foreign('equipo_id')->references('idEquipo')->on('equipos')->onDelete('cascade');

            $table->unsignedInteger('usuario_id');
            $table->foreign('usuario_id')->references('idUsuario')->on('usuarios')->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicituds');
    }
}
