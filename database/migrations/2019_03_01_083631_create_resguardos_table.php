<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResguardosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resguardos', function (Blueprint $table) {
            $table->bigIncrements('idResguardo')->unique();                    
            $table->string('nombreResguardo',200);
            $table->date('fechaInicio')->notNullable();
            $table->date('fechaFin')->notNullable();
            $table->text('observaciones');

            $table->unsignedInteger('solicitud_id');
            $table->foreign('solicitud_id')->references('idSolicitud')->on('solicituds')->onDelete('cascade');

            $table->unsignedInteger('ubicacion_id');
            $table->foreign('ubicacion_id')->references('idUbicacion')->on('ubicacions')->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resguardos');
    }
}
