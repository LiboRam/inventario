<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResguardoEquiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resguardo_equipos', function (Blueprint $table) {
            $table->unsignedInteger('resguardo_id');
            $table->foreign('resguardo_id')->references('idResguardo')->on('resguardos')->onDelete('cascade');
            $table->integer('cantidadEntregado');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resguardo_equipos');
    }
    
}
