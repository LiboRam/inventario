<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleEquiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_equipos', function (Blueprint $table) {
            $table->unsignedInteger('equipo_id');
            $table->foreign('equipo_id')->references('idEquipo')->on('equipos')->onDelete('cascade');
            $table->integer('existencia');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_equipos');
    }
}
