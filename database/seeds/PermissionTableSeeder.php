<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
           'role-list',
           'role-create',
           'role-edit',
           'role-delete',
           'user-list',
           'user-create',
           'user-edit',
           'user-delete',
           'marca-list',
           'marca-create',
           'marca-edit',
           'marca-delete',
           'modelo-list',
           'modelo-create',
           'modelo-edit',
           'modelo-delete',
           'categoria-list',
           'categoria-create',
           'categoria-edit',
           'categoria-delete',
           'unidad-list',
           'unidad-create',
           'unidad-edit',
           'unidad-delete',
           'edoEquipo-list',
           'edoEquipo-create',
           'edoEquipo-edit',
           'edoEquipo-delete',
           'solicitantes-list',
           'solicitantes-create',
           'solicitantes-edit',
           'solicitantes-delete',
           'municipio-list',
           'municipio-create',
           'municipio-edit',
           'municipio-delete',
           'ubicacion-list',
           'ubicacion-create',
           'ubicacion-edit',
           'ubicacion-delete',
           'area-list',
           'area-create',
           'area-edit',
           'area-delete',
           'solicitud-list',
           'solicitud-create',
           'solicitud-edit',
           'solicitud-delete',
           'resguardo-list',
           'resguardo-create',
           'resguardo-edit',
           'resguardo-delete',
           'resguardo_equipos-list',
           'resguardo_equipos-create',
           'resguardo_equipos-edit',
           'resguardo_equipos-delete'
           'equipo-list',
           'equipo-create',
           'equipo-edit',
           'equipo-delete',

        ];


        foreach ($permissions as $permission) {
             Permission::create(['name' => $permission]);
        }
    }
}
