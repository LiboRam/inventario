
(function( $ ) {

	'use strict';

	var datatableInit = function() {
		var $table = $('#datatable-details2e');

		// format function for row details
		var fnFormatDetails = function( datatable, tr ) {
			var data = datatable.fnGetData( tr );

			return [
			'<div class="col-sm-10 col-md-offset-1  col-xs-12">',
			'<table class="table table table-condensed table-responsive" >',
			'<tr class="b-top-none">',
			'<td class="text-dark" ><label class="mb-none">Nombre equipo:</label></td>',
			'<td>' + data[0]+ ' ' + data[6] + '</td>',
			'</tr>',

			'<tr>',
			'<td class="  text-dark"><label class="mb-none">Número serie:</label></td>',
			'<td>' + data[1]+ '</td>',
			'</tr>',

			'<tr>',
			'<td class="  text-dark"><label class="mb-none">Sicipo:</label></td>',
			'<td>' + data[2]+ '</td>',
			'</tr>',

			'<tr>',
			'<td class="  text-dark"><label class="mb-none">Descripción:</label></td>',
			'<td>' + data[3]+ '</td>',
			'</tr>',

			'<tr>',
			'<td class="  text-dark"><label class="mb-none">Marca:</label></td>',
			'<td>' + data[4]+ '</td>',
			'</tr>',

			'<tr>',
			'<td class="  text-dark"><label class="mb-none">Modelo:</label></td>',
			'<td>' + data[5]+ '</td>',
			'</tr>',

			'<tr>',
			'<td class="  text-dark"><label class="mb-none">Categoria:</label></td>',
			'<td>' + data[7]+ '</td>',
			'</tr>',

			'<tr>',
			'<td class="  text-dark"><label class="mb-none">Unidad:</label></td>',
			'<td>' + data[8]+ '</td>',
			'</tr>',

			'<tr>',
			'<td class="  text-dark"><label class="mb-none">Estatus:</label></td>',
			'<td>' + data[9]+ '</td>',
			'</tr>',

			'</div>',
			'</div>'
			].join('');
		};

		// insert the expand/collapse column
		var th = document.createElement( 'th' );
		var td = document.createElement( 'td' );
		td.innerHTML = '<i data-toggle class="fa fa-plus-square-o text-primary h5 m-none" style="cursor: pointer;" title="Ver detalles"></i>';
		td.className = "text-center";

		$table
		.find( 'thead tr' ).each(function() {
			this.insertBefore( th, this.childNodes[0] );
		});

		$table
		.find( 'tbody tr' ).each(function() {
			this.insertBefore(  td.cloneNode( true ), this.childNodes[0] );
		});

		// initialize
		var datatable = $table.dataTable({
			aoColumnDefs: [{
				bSortable: false,
				aTargets: [ 0 ]
			}],
			aaSorting: [
			[1, 'asc']
			]
		});

		// add a listener
		$table.on('click', 'i[data-toggle]', function() {
			var $this = $(this),
			tr = $(this).closest( 'tr' ).get(0);

			if ( datatable.fnIsOpen(tr) ) {
				$this.removeClass( 'fa-minus-square-o' ).addClass( 'fa-plus-square-o' );
				datatable.fnClose( tr );
			} else {
				$this.removeClass( 'fa-plus-square-o' ).addClass( 'fa-minus-square-o' );
				datatable.fnOpen( tr, fnFormatDetails( datatable, tr), 'details' );
			}
		});
	};

	$(function() {
		datatableInit();
	});

}).apply( this, [ jQuery ]);