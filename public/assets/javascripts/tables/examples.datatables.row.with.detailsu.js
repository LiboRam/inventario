
(function( $ ) {

    'use strict';

    var datatableInit = function() {
        var $table = $('#datatable-detailsu');
        

        // format function for row details
        var fnFormatDetails = function( datatable, tr ) {
            var data = datatable.fnGetData( tr );

            return [
            '<div class="col-sm-10 col-md-offset-1  col-xs-12">',
            '<table class="table table-condensed">',
            '<tr class="b-top-none">',
            '<tr>',
            '<td class="p-3 mb-2  text-dark"><label class="mb-none">Nombre de la ubicación:</label></td>',
            '<td>' + data[2]+ '  </td>',
            '</tr>',

            '<tr>',
            '<td class="  text-dark"><label class="mb-none">Calle:</label></td>',
            '<td>' + data[3]+ '  </td>',
            '</tr>',

            '<tr>',
            '<td class="  text-dark"><label class="mb-none">Número interior:</label></td>',
            '<td>' + data[4]+ '  </td>',
            '</tr>',

            '<tr>',
            '<td class="  text-dark"><label class="mb-none">Número exterior:</label></td>',
            '<td>' + data[5]+ '  </td>',
            '</tr>',

            '<tr>',
            '<td class="  text-dark"><label class="mb-none">Colonia:</label></td>',
            '<td>' + data[6]+ '  </td>',
            '</tr>',

            '<tr>',
            '<td class="  text-dark"><label class="mb-none">Municipio:</label></td>',
            '<td>' + data[7]+ '  </td>',
            '</tr>',

            '<tr>',
            '<td class="  text-dark"><label class="mb-none">Latitud:</label></td>',
            '<td>' + data[8]+ '  </td>',
            '</tr>',

            '<tr>',
            '<td class="  text-dark"><label class="mb-none">Longitud:</label></td>',
            '<td>' + data[9]+ '  </td>',
            '</tr>',
            '</div>',
            '</div>',
            ].join('');
        };

        // insert the expand/collapse column
        var th = document.createElement( 'th' );
        var td = document.createElement( 'td' );
        td.innerHTML = '<i data-toggle class="fa fa-plus-square-o text-primary h5 m-none" style="cursor: pointer;" title="Ver detalles"></i>';
        td.className = "text-center";

        $table
        .find( 'thead tr' ).each(function() {
            this.insertBefore( th, this.childNodes[0] );
        });

        $table
        .find( 'tbody tr' ).each(function() {
            this.insertBefore(  td.cloneNode( true ), this.childNodes[0] );
        });

        // initialize
        var datatable = $table.dataTable({
            aoColumnDefs: [{
                bSortable: false,
                aTargets: [ 0 ]
            }],
            aaSorting: [
            [1, 'asc']
            ]
        });

        // add a listener
        $table.on('click', 'i[data-toggle]', function() {
            var $this = $(this),
            tr = $(this).closest( 'tr' ).get(0);

            if ( datatable.fnIsOpen(tr) ) {
                $this.removeClass( 'fa-minus-square-o' ).addClass( 'fa-plus-square-o' );
                datatable.fnClose( tr );
            } else {
                $this.removeClass( 'fa-plus-square-o' ).addClass( 'fa-minus-square-o' );
                datatable.fnOpen( tr, fnFormatDetails( datatable, tr), 'details' );
            }
        });
    };

    $(function() {
        datatableInit();
    });

}).apply( this, [ jQuery ]);