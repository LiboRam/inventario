
(function( $ ) {

	'use strict';

	var datatableInit = function() {
		var $table = $('#datatable-detailsus');

		// format function for row details
		var fnFormatDetails = function( datatable, tr ) {
			var data = datatable.fnGetData( tr );

			return [
			'<div class="col-sm-10 col-md-offset-1  col-xs-12">',
			'<table class="table table table-condensed">',
			'<tr class="b-top-none">',
			'<tr>',
			'<td class="text-dark" ><label class="mb-none">Usuario:</label></td>',
			'<td>' + data[2]+ '</td>',
			'</tr>',
			'<tr>',
			'<td class="  text-dark"><label class="mb-none">Nombre del usuario:</label></td>',
			'<td>' + data[3]+ '</td>',
			'</tr>',
			'<tr>',
			'<td class="  text-dark"><label class="mb-none">Contraseña:</label></td>',
			'<td>' + data[4]+ '</td>',
			'</tr>',
			'<tr>',
			'<td class="  text-dark"><label class="mb-none">Puesto:</label></td>',
			'<td>' + data[5]+ '</td>',
			'</tr>',
			'<tr>',
			'<td class="  text-dark"><label class="mb-none">Fecha de creacion:</label></td>',
			'<td>' + data[6]+ '</td>',
			'</tr>',
			'<tr>',
			'<td class="  text-dark"><label class="mb-none">Area:</label></td>',
			'<td>' + data[7]+ '</td>',
			'</tr>',
			'</div>',
			'</div>'
			].join('');
		};

		// insert the expand/collapse column
		var th = document.createElement( 'th' );
		var td = document.createElement( 'td' );
		td.innerHTML = '<i data-toggle class="fa fa-plus-square-o text-primary h5 m-none" style="cursor: pointer;" title="Ver detalles"></i>';
		td.className = "text-center";

		$table
		.find( 'thead tr' ).each(function() {
			this.insertBefore( th, this.childNodes[0] );
		});

		$table
		.find( 'tbody tr' ).each(function() {
			this.insertBefore(  td.cloneNode( true ), this.childNodes[0] );
		});

		// initialize
		var datatable = $table.dataTable({
			aoColumnDefs: [{
				bSortable: false,
				aTargets: [ 0 ]
			}],
			aaSorting: [
			[1, 'asc']
			]
		});

		// add a listener
		$table.on('click', 'i[data-toggle]', function() {
			var $this = $(this),
			tr = $(this).closest( 'tr' ).get(0);

			if ( datatable.fnIsOpen(tr) ) {
				$this.removeClass( 'fa-minus-square-o' ).addClass( 'fa-plus-square-o' );
				datatable.fnClose( tr );
			} else {
				$this.removeClass( 'fa-plus-square-o' ).addClass( 'fa-minus-square-o' );
				datatable.fnOpen( tr, fnFormatDetails( datatable, tr), 'details' );
			}
		});
	};

	$(function() {
		datatableInit();
	});

}).apply( this, [ jQuery ]);