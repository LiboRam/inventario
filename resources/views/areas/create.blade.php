@extends('templates.master')
@section('content')

<header class="panel-heading">
    <h2 class="panel-title">Insertar datos del area</h2>
</header>
<div class="panel-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="mb-md">

                <a href="{{url ('areas') }}" class="btn btn-primary mt-4 ml-3">Regresar 
                </a>
            </div>
        </div>
    </div>

    {!! Form::open(['idArea' => 'dataForm', 'url' => '/areas','data-parsley-validate']) !!}
    <div class="col-sm-8 col-md-offset-2  col-xs-12">
        <div class="form-group">
            {!! Form::label('nombreArea', 'Nombre área:'); !!}
            {!! Form::text('nombreArea', null, ['placeholder' => 'Agrega un area', 'class' => 'form-control' ,'required',
            'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
        </div>
    </div>
</div>
<footer class="panel-footer">
    {!! Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary active')) !!} 
    {!! link_to(URL::previous(), 'Cancelar', ['class' => 'btn btn-default active',]) !!} 
</footer>
{!! Form::close() !!}


@endsection()

