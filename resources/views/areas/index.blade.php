@extends('templates.master')

@section('content')
<header class="panel-heading">
  <h2 class="panel-title">Listado de areas</h2>
</header>
<div class="panel-body">
  <div class="row">
   <div class="col-lg-12 col-md-10 col-sm-10 col-xs-12">
    <div class="mb-md">
      <a href="{{url ('areas/create') }}" class="btn btn-primary mt-4 ml-3"> <i class="fa fa-plus">Agregar</i>  
      </a>
    </div>
  </div>
</div>

@if(Session::has('message'))
<div class="col-sm-10 col-md-offset-1  col-xs-12">
  <div class="alert alert-default alert-dismissible mt-5">
    <strong>{!! Session('message') !!}</strong>
    <button type="button" class="close" data-dismiss="alert">
      <span>x</span>
    </button>
  </div>
</div>
@endif()
<div class="col-sm-10 col-md-offset-1  col-xs-12">
  <table class="table table-bordered table-hover mb-none" id="datatable-default">
    <thead>
      <tr>
        <th width="110px;" >Clave</th>
        <th>
          <?php echo utf8_decode("Nombre área");?>
        </th>
        <th width="130px;">
          <?php echo utf8_decode("Acción");?>
        </th>
      </tr>
    </thead>
    <tbody>

      @foreach($areas as  $area)
      <tr class="areas{{$area->idArea}}">
        <td>{{ $area->idArea  }}</td>
        <td>{{ $area->nombreArea }}</td>

        <td class="actions">
         {!! Form::open(['route'=>['areas.destroy', $area->idArea],'method'=>'DELETE', 'class' => 'form-horizontal','role' => 'form', 'onsubmit' => 'return  ConfirmDelete()']) !!}                                            
         <a href="areas/{!! $area->idArea !!}/edit" class="btn btn-default btn-sm" role="button"><i class="fa fa-pencil"></i></a>

         <button type="submit" name="button"  class="btn btn-default btn-sm">
          <i class="fa fa-trash-o"></i>
        </button>
        {!! Form::close() !!} 
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
</section>
<div class="col-lg-4 col-md-10 col-sm-10 col-xs-12">
  <script type="text/javascript">
    function ConfirmDelete()
    {
      var x = confirm("Estas seguro de Eliminar?");
      if (x)
        return true;
      else
        return false;
    } 
  </script>
</div>
<!--<script type="text/javascript">
 function mensaje(){
wal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {
    Swal.fire(
      'Deleted!',
      'Your file has been deleted.',
      'success'
    )
  }
})
}
</script> -->
@endsection()           