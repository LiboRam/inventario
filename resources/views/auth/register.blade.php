@extends('templates.master')

@section('content')

    <header class="panel-heading">
        <h2 class="panel-title">Registro de usuarios</h2>
    </header>

        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="mb-md">
                        <a href="{{url ('usuarios') }}" class="btn btn-primary" style="margin-bottom: 15px;">Regresar  
                        </a>
                    </div>
                </div>
            </div>

                <div class="card-body">
                    <form method="POST" role="form" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        @csrf

                        <div class="column">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="nombreUsuario">{{ __('Nombre') }}</label>

                                    <input id="nombreUsuario" type="text" class="form-control{{ $errors->has('nombreUsuario') ? ' is-invalid' : '' }}" name="nombreUsuario" value="{{ old('nombreUsuario') }}" required>

                                        @if ($errors->has('nombreUsuario'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('nombreUsuario') }}</strong>
                                            </span>
                                        @endif
                                </div>
                            </div>
                        </div>
                        
                        <div class="column">
                            <div class="clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="apellidoP">{{ __('Apellido paterno') }}</label>

                                        <input id="apellidoP" type="text" class="form-control{{ $errors->has('apellidoP') ? ' is-invalid' : '' }}" name="apellidoP" value="{{ old('apellidoP') }}" required>

                                            @if ($errors->has('apellidoP'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('apellidoP') }}</strong>
                                                </span>
                                            @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="column">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="apellidoM">{{ __('Apellido materno') }}</label>

                                    <input id="apellidoM" type="text" class="form-control{{ $errors->has('apellidoM') ? ' is-invalid' : '' }}" name="apellidoM" value="{{ old('apellidoM') }}" required>

                                        @if ($errors->has('apellidoM'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('apellidoM') }}</strong>
                                            </span>
                                        @endif
                                </div>
                            </div>
                        </div>

                        <div class="column">
                            <div class="clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="puesto">{{ __('Puesto') }}</label>

                                        <input id="puesto" type="text" class="form-control{{ $errors->has('puesto') ? ' is-invalid' : '' }}" name="puesto" value="{{ old('puesto') }}" required>

                                            @if ($errors->has('puesto'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('puesto') }}</strong>
                                                </span>
                                            @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="column">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="area_id">{{ __('Area') }}</label>

                                    <select id="area_id" class="form-control{{ $errors->has('area_id') ? ' is-invalid' : '' }}" name="area_id" value="{{ old('area_id') }}" required>
                                    </select>

                                     @if ($errors->has('area_id'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('area_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="column">
                                <div class="clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="fechaCreacion">{{ __('Fecha') }}</label>
                                               
                                            <input id="fechaCreacion" type="date" class="form-control{{ $errors->has('fechaCreacion') ? ' is-invalid' : '' }}" name="fechaCreacion" value="{{ old('fechaCreacion') }}" required>

                                                @if ($errors->has('fechaCreacion'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('fechaCreacion') }}</strong>
                                                    </span>
                                                @endif
                                        </div>
                                    </div>
                                </div>
                        </div>

                        <div class="column">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label for="username">{{ __('Usuario') }}</label>
                                        
                                        <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>

                                            @if ($errors->has('username'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('username') }}</strong>
                                                    </span>
                                            @endif
                                    </div>
                                </div>
                        </div>

                        <div class="column">
                                <div class="clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="password">{{ __('Contraseña') }}</label>

                                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                                @if ($errors->has('password'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                        </div>
                                    </div>
                                </div>
                        </div>

                        <div class="column">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="password-confirm" class="col-lg-6 col-md-6 col-sm-6 col-xs-12">{{ __('Confirm Password') }}</label>

                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>
                        </div>

                       
                         
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <button type="submit" class="btn btn-primary active">
                                    {{ __('Crear') }}
                                </button>
                            </div>
                        
                    


                       
                    </form>
                </div>
            </div>

            <footer class="panel-footer">
                
            </footer>

@endsection
