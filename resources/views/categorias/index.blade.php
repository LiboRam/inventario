@extends('templates.master')
@section('content')
<!-- start: page -->
<header class="panel-heading">
  <span class="separator">
    <h2 class="panel-title">Listado de categorias</h2>
  </span>
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-5">
      <div class="mb-md">
        <a href="{{url ('categorias/create') }}" class="btn btn-primary mt-4 ml-3"> <i class="fa fa-plus">Agregar</i>  
        </a>
      </div>
    </div>
  </div>

  @if(Session::has('message'))
  <div class="col-sm-10 col-md-offset-1  col-xs-12">
    <div class="alert alert-default alert-dismissible mt-5">
      <strong>{!! Session('message') !!}</strong>
      <button type="button" class="close" data-dismiss="alert">
        <span>x</span>
      </button>
    </div>
  </div>
  @endif()
  <div class="col-sm-10 col-md-offset-1  col-xs-12">
    <table class="table table-bordered table-hover table-striped mb-none" id="datatable-default">
      <thead>
        <tr>
          <th width="80px;" >Clave</th>
          <th>Nombre categoria</th>
          <th width="80px;">
         </th>
       </tr>
     </thead>
     <tbody>

      @foreach($categorias as  $cat)
      <tr class="categorias{{$cat->idCategoria}}">
        <td>{{ $cat->idCategoria  }}</td>
        <td>{{ $cat->nombreCategoria  }}</td>
        <td class="actions">

          {!! Form::open(['route'=>['categorias.destroy', $cat->idCategoria],'method'=>'DELETE', 'class' => 'form-horizontal','role' => 'form' , 'onsubmit' => 'return ConfirmDelete()']) !!}
          <a href="categorias/{!! $cat->idCategoria !!}/edit" class="btn btn-default btn-sm" title="Editar" role="button"><i class="fa fa-pencil"></i></a>    

          <button type="submit" name="button" class="btn btn-default btn-sm" title="Eliminar">
            <i class="fa fa-trash-o"></i>
          </button> 

          {!! Form::close() !!}     
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
</div>
@endsection()

<script type="text/javascript">
  function ConfirmDelete()
  {
    var x = confirm("Estas seguro de Eliminar?");
    if (x)
      return true;
    else
      return false;
  } 
</script>