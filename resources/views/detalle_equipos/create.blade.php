@extends('templates.master')

@section('content')

<!--- -->
<header class="panel-heading">
    <h2 class="panel-title">Insertar datos</h2>
</header>
<div class="panel-body">
    <div class="row">
        <div class="col-sm-12">
          <!--  <div class="mb-md">
                <a href="{{url ('detalle_equipos') }}" class="btn btn-primary mt-4 ml-3">Regresar  
                </a>
            </div>-->
            <a href="{{url ('equipos') }}" class="btn btn-primary mt-4 ml-3">Regresar  
        </a>
         <div class="row" id="C" @if(old('condicion') != 'C') style="display: none;" @endif>
        <a href="{{url ('detalle_equipos/create') }}" class="btn btn-primary mt-4 ml-3 " id="dialog"  >Nuevo equipo</i>  
        </a>
      </div>
        </div>
    </div>

    {!! Form::open(['equipo_id' => 'dataForm', 'url' => '/detalle_equipos','data-parsley-validate']) !!}

    <div class="col-sm-8 col-md-offset-2  col-xs-12">
        <div class="form-group">
            {!! Form::label('equipo_id', 'Equipo:'); !!}
            {!! Form::select('equipo_id', $equipo->pluck('nombreEquipo', 'idEquipo'), null, ['class' => 'form-control','required']); !!}
        </div>
    </div>
    <div class="col-sm-8 col-md-offset-2  col-xs-12">
        <div class="form-group">
            {!! Form::label('existencia', 'existencia:'); !!}
            {!! Form::input('numeric', 'existencia', null, ['class' => 'form-control','required']); !!}
        </div>
    </div>

    {!! Form::submit('Guardar', ['class' => 'btn btn-primary pull-right']); !!}

    {!! Form::close() !!}
</div>

@endsection()

<script>
  $(document).ready(function(){
   $('form').parsley();
});
</script>