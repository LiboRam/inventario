@extends('templates.master')

@section('content')

<header class="panel-heading">
    <h2 class="panel-title">Actualizar datos</h2>
</header>
<div class="panel-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="mb-md">

                <a href="{{url ('detalle_equipos') }}" class="btn btn-primary mt-4 ml-3">Leer datos 
                </a>
            </div>
        </div>
    </div>

    {!! Form::open(['equipo_id' => 'dataForm', 'method' => 'PATCH', 'url' => '/detalle_equipos/' . $detalle_equipos->equipo_id,'data-parsley-validate']) !!}
    <div class="col-sm-8 col-md-offset-2  col-xs-12">
        <div class="form-group">
            {!! Form::label('equipo_id', 'Equipo:'); !!}
            {!! Form::select('equipo_id', $equipo->pluck('nombreEquipo', 'idEquipo'), null, ['class' => 'form-control','required']); !!}
        </div>
    </div>
    <div class="col-sm-8 col-md-offset-2  col-xs-12">
        <div class="form-group">
            {!! Form::label('existencia', 'existencia:'); !!}
            {!! Form::number('existencia', $detalle_equipos->existencia, ['placeholder' => 'en existencia', 'class' => 'form-control','required']); !!}
        </div>
    </div>

    {!! Form::submit('Actualizar', ['class' => 'btn btn-primary pull-right']); !!}

    {!! Form::close() !!}
</div>
@endsection()