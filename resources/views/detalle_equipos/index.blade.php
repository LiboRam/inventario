@extends('templates.master')
@section('content')
<header class="panel-heading">
  <span class="separator">
    <h2 class="panel-title">Listado de equipos terminados</h2>
  </span>
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-lg-12 col-md-10 col-sm-10 col-xs-12">
      <div class="mb-md">
        <a href="{{url ('detalle_equipos/create') }}"  class="btn btn-default mt-4 ml-3 pull-right" title="Descargar PDF"> <i class="fa fa-file-pdf-o">PDF</i>  
        </a>
        <br>
      </div>
    </div>
  </div>

  @if(Session::has('message'))
  <div class="alert alert-default alert-dismissible mt-5">
    <strong>{!! Session('message') !!}</strong>
    <button type="button" class="close" data-dismiss="alert">
      <span>x</span>
    </button>
  </div>
  @endif()

  <table class="table table-bordered table-hover mb-none table-striped" id="datatable-default">
    <thead>
      <tr>
        <th style="padding-left: 15px;">Nombre del equipo</th>
        <th>Numero de serie</th>
        <th class="hide" >Sicipo</th>
        <th>Descripcion</th>
        <th class="hide" >Marca</th>
        <th class="hide" >Modelo</th>
        <th class="hide" >Categoria</th>
        <th class="hide" >Unidad</th>
        <th class="hide" >Estatus</th>
        <th class="hide" >Fecha de ingreso</th>
        <th>Existencia</th>
      </tr>
    </thead>
    <tbody>

      @foreach($detalle_equipos as $detalle)
      <tr>
        <td style="padding-left: 15px;">{!! $detalle->Equipo->nombreEquipo !!}</td>
        <td>{{ $detalle->Equipo->numSerie }}</td>
        <td class="hide" >{{ $detalle->Equipo->sicipo }}</td>
        <td>{{ $detalle->Equipo->descripcion }}</td>
        <td class="hide" >{{ $detalle->Equipo->Marca->nombreMarca }}</td>
        <td class="hide" >{{ $detalle->Equipo->Modelo->nombreModelo }}</td>
        <td class="hide" >{{ $detalle->Equipo->Categoria->nombreCategoria }}</td>
        <td class="hide" >{{ $detalle->Equipo->Unidad->nombreUnidad }}</td>
        <td class="hide" >{{ $detalle->Equipo->EdoEquipos->nombreUnidad }}</td>
        <td class="hide" >{{ $detalle->Equipo->fechaIngreso }}</td>
        <td width="15%">{!! $detalle->existencia !!}</td>
      </tr>
      @endforeach

    </tbody>
  </table>
</div>
@endsection()

