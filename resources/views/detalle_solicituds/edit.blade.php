@extends('templates.master')

@section('content')
    <section  class="content-body">
   <header class="panel-heading">
    <h2 class="panel-title">Actualizar datos</h2>
</header>
<div class="panel-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="mb-md">

                <a href="{{url ('detalle_solicituds') }}" class="btn btn-primary mt-4 ml-3">Leer datos 
                </a>
            </div>
        </div>
    </div>

    {!! Form::open(['solicitud_id' => 'dataForm', 'method' => 'PATCH', 'url' => '/detalle_solicituds/' . $detalle_solicituds->solicitud_id,'data-parsley-validate']) !!}
     <div class="col-sm-8 col-md-offset-2  col-xs-12">
    <div class="form-group">
        {!! Form::label('solicitud_id', 'Solicitud:'); !!}
        {!! Form::select('solicitud_id', $solicitud->pluck('nombreSolicitud', 'idSolicitud'), null, ['class' => 'form-control','placeholder' => 'Seleccione','required',
                'data-parsley-required-message' =>'Seleccione una lista']); !!}
    </div>
</div>
 <div class="col-sm-8 col-md-offset-2  col-xs-12">
    <div class="form-group">
        {!! Form::label('equipo_id', 'equipo:'); !!}
        {!! Form::select('equipo_id', $equipo->pluck('nombreEquipo', 'idEquipo'), null, ['class' => 'form-control','placeholder' => 'Seleccione','required',
                'data-parsley-required-message' =>'Seleccione una lista']); !!}
    </div>
</div>
    {!! Form::submit('Actualizar', ['class' => 'btn btn-primary pull-right']); !!}

    {!! Form::close() !!}
</div>
</section>
@endsection()

<script>
  $(document).ready(function(){
     $('form').parsley();
 });
</script>