 @extends('templates.master')

 @section('content')
 <header class="panel-heading">
  <h2 class="panel-title">Insertar datos del equipo</h2>
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-12">
      <div class="mb-md">

        <a href="{{url ('equipos') }}" class="btn btn-primary mt-4 ml-3">Regresar  
        </a>
      </div>
    </div>
  </div>

  {!! Form::open(['route'=>'equipos.store', 'method'=>'STORE', 'files' => true,'data-parsley-validate' ,'role' => 'form']) !!} 

  @include('equipos.frm.form')   
  
  {!! Form::close() !!}
</div>
</div>

@endsection()

<script>
  $(document).ready(function(){
   $('form').parsley();
 });
</script>
