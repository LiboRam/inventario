@extends('templates.master')

@section('content')  
<header class="panel-heading">
  <h2 class="panel-title">Actualizar datos del equipo</h2>
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-12">
      <div class="mb-md">
        <a href="{{url ('equipos') }}" class="btn btn-primary mt-4 ml-3">Regresar 
        </a>
      </div>
    </div>
  </div>

  {!! Form::model($equipo,['route'=>['equipos.update', $equipo->idEquipo],'files' => true, 'method'=>'PUT', 'role' => 'form']) !!} 

  @include('equipos.frm.form')
  
  {!! Form::close() !!}

</div>
</section>
@endsection()