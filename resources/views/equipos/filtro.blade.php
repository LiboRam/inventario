@extends('templates.master')

@section('content')
<!-- start: page -->
<header class="panel-heading" >
  <h2 class="panel-title">Informe </h2>
</header>
<div class="panel-body">
  <div class="row">
   <div class="col-lg-12 col-md-10 col-sm-10 col-xs-12">
    <div class="mb-md">
      <a href="{{url ('equipos/create') }}"  class="btn btn-primary mt-4 ml-3"> <i class="fa fa-plus">Agregar</i>
      </a>
    </div>
  </div>
</div>

@if(Session::has('message'))
<div class="alert alert-danger alert-dismissible mt-5">
  <strong>{!! Session('message') !!}</strong>
  <button type="button" class="close" data-dismiss="alert">
    <span>x</span>
  </button>
</div>
@endif() 

<table class="table table-bordered table-hover mb-none table-striped" id="datatable-details">
  <thead>
    <tr>
      <th class="hide">Id</th>
      <th width="115px;">Nombre equipo</th>
      <th width="115px;">Nombre ubicacion</th>
      <th width="115px;">Nombre solicitante</th>
    </tr>
  </thead>
  <tbody>

    @foreach( $equipos as  $equipo)
    <tr class="equipos{{$equipo->idEquipo}}">
     <td class="hide">{{ $equipo->idEquipo }}</td>
     <td>{{ $equipo->nombreEquipo }}</td>
     <td>{{ $equipo->Ubicacion->nombreUbicacion }}</td>
     <td>{{ $equipo->Solicitante->nombreSolicitante }}</td>
   </tr>
   @endforeach

 </tbody>
</table>
</div>
@endsection()