  <!-- <div class="container-fluid"> -->
    <div class="panel-body">
     <div class="column">
       <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 control-label">
        <div class="form-group">
          {!! Form::label('nombreEquipo', 'Nombre equipo:'); !!}
          {!! Form::text('nombreEquipo', null, ['placeholder' => 'Nombre del equipo',
          'class' => 'form-control',
          'required',
          'data-parsley-required-message' =>'Por favor rellene este campo' ]); !!}
        </div>
      </div>
    </div>

  <div class="column">
   <div class="clearfix">
     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 control-label">
      <div class="form-group">
        {!! Form::label('numSerie', 'No serie:'); !!}
        {!! Form::text('numSerie', null, ['placeholder' => 'Numero de serie del equipo', 'class' => 'form-control','required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
      </div>
    </div>
  </div>
</div>

<div class="column">
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 control-label">
  <div class="form-group">
    {!! Form::label('sicipo', 'Sicipo:'); !!}
    {!! Form::text('sicipo', null, ['placeholder' => 'sicipo del equipo', 'class' => 'form-control','required', 'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
  </div>
</div>
</div>


<div class="column">
 <div class="clearfix">
   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 control-label">
    <div class="form-group">
      {!! Form::label('descripcion', 'Descripcion:'); !!}
      {!! Form::textarea('descripcion', null, ['placeholder' => 'descripcion del equipo', 'class' => 'form-control','rows' => '1','required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
    </div>
  </div>
</div>
</div>

<div class="column">
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 control-label">
  <div class="form-group">
    {!! Form::label('fechaIngreso', 'Fecha de ingreso:'); !!}
    {!! Form::input('date','fechaIngreso', date('Y-m-d'), ['placeholder' => 'fecha', 'class' => 'form-control','required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
  </div>
</div>
</div>



<div class="column">
 <div class="clearfix">
   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 control-label">
     <div class="form-group">
      {!! Form::label('foto', 'Selecciona una imagen:', array('class' => 'negrita')) !!}                          
      <div>
        {!! Form::file('foto',null, array('required' => 'true')) !!}
        <br><br>                  

        @if ( !empty ( $equipo->foto) )

        {!! Form::label('foto', 'Imagen actual:', array('class' => 'negrita')) !!}

        <img src="{!! asset("images/$equipo->foto") !!}" width="90"  class="img-fluid img-thumbnail" >                                  

        @else

        <!-- Aun no se ha cargado una imagen para este equipo-->

        @endif 

      </div>
    </div>
  </div>
</div>
</div>

<div class="column">
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 control-label">
  <div class="form-group">
    {!! Form::label('marca_id', 'Marca:'); !!}
    {!! Form::select('marca_id', $marca->pluck('nombreMarca', 'idMarca'), null, ['class' => 'form-control','placeholder' => '--selecciona--','required',
    'data-parsley-required-message' =>'Selecciona una marca']); !!}
  </div>
</div>
</div>

<div class="column">
  <div class="clearfix">
   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 control-label">
    <div class="form-group">
      {!! Form::label('modelo_id', 'Modelo:'); !!}
      {!! Form::select('modelo_id', $modelo->pluck('nombreModelo', 'idModelo'), null, ['class' => 'form-control','placeholder' => '--selecciona--','required',
      'data-parsley-required-message' =>'Selecciona un modelo']); !!}
    </div>
  </div>
</div>
</div>

<div class="column">
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 control-label">
  <div class="form-group">
    {!! Form::label('categoria_id', 'Categoria:'); !!}
    {!! Form::select('categoria_id', $categoria->pluck('nombreCategoria', 'idCategoria'), null, ['class' => 'form-control','placeholder' => '--selecciona--','required',
    'data-parsley-required-message' =>'Selecciona una categoria']); !!}
  </div>
</div>
</div>

<div class="column">
  <div class="clearfix">
   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="form-group">
      {!! Form::label('unidad_id', 'Unidad de medida:'); !!}
      {!! Form::select('unidad_id', $unidad->pluck('nombreUnidad', 'idUnidad'), null, ['class' => 'form-control','placeholder' => '--selecciona--','required',
      'data-parsley-required-message' =>'Selecciona una unidad']); !!}
    </div>
  </div>
</div>
</div>

<div class="column">
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
  <div class="form-group">
    {!! Form::label('edo_id', 'Estado del equipo:'); !!}
    {!! Form::select('edo_id', $edoequipo->pluck('nombreEdo', 'idEdo'), null, ['class' => 'form-control','placeholder' => '--selecciona--','required',
    'data-parsley-required-message' =>'Selecciona un estado']); !!}
  </div>
</div>
</div>
</div>
<footer class="panel-footer">
  {!! Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary active')) !!} 
  {!! link_to(URL::previous(), 'Cancelar', ['class' => 'btn btn-default active ',]) !!} 
</footer>
