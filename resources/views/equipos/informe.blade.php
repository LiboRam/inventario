<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Informe de los equipos</title>

	<?php $image_path = '/images/imgInf.jpg';  ?>
	<img src="{{ public_path() . $image_path }}" style="width:728px; height:100px; align:right">
	
	<style type="text/css">

		table{
			font-family: arial, sans-serif;
			border-collapse: collapse;
			width: 100%;
		}

		td, th{
			border: 1px solid #000000;
			text-align: left;
			padding: 8px;
		}
		th {
			background-color: #dddddd;
			text-align: center;
		}

  /* h2 {
     text-align: center;
     } */
     body {
     	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
     	font-size: 14px;
     	line-height: 1.42857143;
     	color: #333333;
     	background-color: #ffffff;
     }
 </style> 
</head>
<body>
	<p>Fecha impreso: {{$today}}</p>
	<h2>Listado de equipos</h2>
	<table class="table table-bordered table-hover mb-none table-striped">
		<thead>
			<tr>
				<th>Id</th>
				<th width="100px;">Nombre equipo</th>
				<th width="50px;">Número serie
				</th>
				<th width="50px;">Sicipo</th>
				<th width="80px;">Descripcion</th>
				<th width="30px;">Marca</th>
				<th width="30px;">Modelo</th>
				<th width="25px;">Categoria</th>
				<th width="15px;">Unidad</th>
				<th width="15px;">Estatus</th>
				<th width="15px;">Fecha ingreso</th>
			</tr>
		</thead>
		<tbody>

			@foreach( $equipos as  $equipo)
			<tr> 
				<td>{{ $equipo->idEquipo }}</td>
				<td>{{ $equipo->nombreEquipo }}</td>
				<td>{{ $equipo->numSerie }}</td>
				<td>{{ $equipo->sicipo }}</td>
				<td>{{ $equipo->descripcion }}</td>
				<td>{{ $equipo->Marca->nombreMarca }}</td>
				<td>{{ $equipo->Modelo->nombreModelo}}</td>
				<td>{{ $equipo->Categoria->nombreCategoria }}</td>
				<td>{{ $equipo->Unidad->nombreUnidad }}</td>
				<td>{{ $equipo->EdoEquipos->nombreEdo }}</td>
				<td>{{ $equipo->fechaIngreso}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>

