@extends('templates.master')

@section('content')
<!-- start: page -->
<header class="panel-heading" >
  <h2 class="panel-title">Listado de equipos terminados</h2>
</header>
<div class="panel-body">
  <div class="row">
   <div class="col-lg-12 col-md-10 col-sm-10 col-xs-12">

    <div class="mb-md">
      <!-- Button trigger modal -->
      <div class="row">
        <div class="col-sm-12">
          <div class="mb-md">
            <button class="btn btn-primary waves-effect waves-light"
            data-toggle="modal"  data-target="#myModal">Nuevo equipo</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@if(Session::has('message'))
<div class="alert alert-danger alert-dismissible mt-5">
  <strong>{!! Session('message') !!}</strong>
  <button type="button" class="close" data-dismiss="alert">
    <span>x</span>
  </button>
</div>
@endif()
<table class="table table-bordered table-hover mb-none table-striped" id="datatable-details">
  <thead>
    <tr>
      <th class="hide">Id</th>
      <th width="115px;">Nombre del equipo</th>
      <th width="90px;" class="hide">
        <?php echo utf8_decode("Número serie");?>
      </th>
      <th class="hide">Sicipo</th>
      <th>Marca</th>
      <th>Modelo</th>
      <th class="hide">Categoria</th>
      <th class="hide">Unidad</th>
      <th class="hide">Estatus</th>
      <th>Imagen</th>
      <th class="hide">Fecha ingreso</th>
      <th>Stock</th>
    </tr>
  </thead>
  <tbody>

    @foreach( $equipos as  $eq)
    <tr class="equipos{{$eq->idEquipo}}">
     <td class="hide">{{ $eq->idEquipo }}</td>
     <td>{{ $eq->nombreEquipo }}</td>
     <td class="hide">{{ $eq->numSerie }}</td>
     <td class="hide">{{ $eq->sicipo }}</td>
     <td>{{ $eq->Marca->nombreMarca}}</td>
     <td>{{ $eq->Modelo->nombreModelo}}</td>
     <td class="hide">{{ $eq->Categoria->nombreCategoria }}</td>
     <td class="hide">{{ $eq->Unidad->nombreUnidad }}</td>
     <td class="hide">{{ $eq->EdoEquipos->nombreEdo }}</td>
     <td>
       <img src="{!! asset("images/$eq->foto") !!}" width="50"  class="img-fluid"> 
     </td>
     <td class="hide">{{ $eq->fechaIngreso}}</td>
     <td>{{$eq->DetalleEquipos->existencia}} </td>
   </tr>
   @endforeach

 </tbody>
</table>
</div>

<!-- modal-->
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content p-0 b-0">
      <div class="panel panel-color panel-primary">
        <div class="panel-heading">
          <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true" title="Cerrar">x</button>
          <h3 class="panel-title">Nuevo equipo terminado</h3>
        </div>
        <div class="panel-body">

          {!! Form::open(['equipo_id' => 'dataForm', 'url' => '/detalle_equipos','data-parsley-validate']) !!}

          <div class="col-sm-8 col-md-offset-2  col-xs-12">
            <div class="form-group">
              {!! Form::label('equipo_id', 'Nombre del equipo:'); !!}
              {!! Form::select('equipo_id', $equipos->pluck('nombreEquipo', 'idEquipo'), null, ['placeholder'=> '--Seleccionar--','class' => 'form-control','required','data-parsley-required-message' =>'Seleccione una lista']); !!}
            </div>
          </div>
          <div class="col-sm-8 col-md-offset-2  col-xs-12">
            <div class="form-group">
              {!! Form::label('existencia', 'Cantidad:'); !!}
              {!! Form::input('numeric', 'existencia', null, ['class' => 'form-control','required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
            </div>
          </div>
        </div> 
      </div>
      <footer class="panel-footer">
        {!! Form::submit('Guardar', ['class' => 'btn btn-primary']); !!}
        {!! link_to(URL::previous(), 'Cancelar', ['class'=> 'btn btn-deafualt active',]) !!}
      </footer>
      {!! Form::close() !!}
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection()

<script>
  $(document).ready(function(){
   $('form').parsley();
 });
</script>