@extends('templates.master')
@section('content')

        <header class="panel-heading">
          <h2 class="panel-title" align="center">Mapa de registro de ubicaciones</h2>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
          <meta name="viewport" content="initial-scale=1.0, user-scalable=no">

            <meta charset="utf-8">
            <style type="text/css">
                #map {
                    height: 100%;
                }
                html, body{
                    height: 100%;
                    margin: 0;
                    padding: 0;
                }
            </style>
        </header>
        <div class="panel-body">
			<div id="map"></div>
		</div>

			<script>

            function initMap() {
               var centro = {lat: 17.05, lng: -96.7167};

                var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 14,
                  center: centro
                });

                var marker = new google.maps.Marker({
                  position: centro,
                  map: map
                });

            var geocoder = new google.maps.Geocoder();
     
            document.getElementById('submit').addEventListener('click', function() {
                geocodeAddress(geocoder, map);
            });
        }

           
        </script>

        
        <script async defer
            src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&callback=initMap&libraries=places&initAutocomplete">
        </script> 


@endsection
						