@extends('templates.master')
@section('content')

   <header class="panel-heading">
    <h2 class="panel-title">Actualizar datos del modelo</h2>
</header>
<div class="panel-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="mb-md">

                <a href="{{url ('modelos') }}" class="btn btn-primary mt-4 ml-3">Regresar 
                </a>
            </div>
        </div>
    </div>
    {!! Form::open(['idModelo' => 'dataForm', 'method' => 'PATCH', 'url' => '/modelos/' . $modelo->idModelo,'data-parsley-validate' ]) !!}

   <div class="col-sm-8 col-md-offset-2  col-xs-12">
    <div class="form-group">
        {!! Form::label('nombreModelo', 'Modelo:'); !!}
        {!! Form::text('nombreModelo', $modelo->nombreModelo, ['placeholder' => 'Modelo', 'class' => 'form-control']); !!}
    </div>
</div>
</div>
<footer class="panel-footer">
    {!! Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary active')) !!} 
    {!! link_to(URL::previous(), 'Cancelar', ['class' => 'btn btn-default active',]) !!} 
</footer>
@endsection()