@extends('templates.master')

@section('content')
<header class="panel-heading">
  <h2 class="panel-title">Insertar datos</h2>
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-12">
      <div class="mb-md">

        <a href="{{url ('municipios') }}" class="btn btn-primary" style="margin-bottom: 15px;">Regresar  
        </a>
      </div>
    </div>
  </div>

  {!! Form::open(['idMunicipio' => 'dataForm', 'url' => '/municipios','data-parsley-validate']) !!}

  <form class="container">
    <div class="row">
     <div class="col-sm-8 col-md-offset-2  col-xs-12">
      <div class="form-group">
        {!! Form::label('clave', 'Clave:'); !!}
        {!! Form::text('clave', null, ['placeholder' => 'Agregar clave', 'class' => 'form-control','required',
        'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
      </div>
    </div>
  </div>

  <div class="row">
   <div class="col-sm-8 col-md-offset-2  col-xs-12">
    <div class="form-group">
      {!! Form::label('nombreMunicipio', 'Municipio:'); !!}
      {!! Form::text('nombreMunicipio', null, ['placeholder' => 'Nombre Municipio', 'class' => 'form-control','required',
      'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
    </div>
  </div>
</div>

<div class="row">
 <div class="col-sm-8 col-md-offset-2  col-xs-12">
   <div class="form-group">
    {!! Form::label('region', 'Region:'); !!}
    {!! Form::text('region', null, ['placeholder' => 'Nombre Region', 'class' => 'form-control','required',
    'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
  </div>
</div>
</div>
<br>
<div class="col-sm-8 col-md-offset-2  col-xs-12">
  {!! Form::submit('Guardar', ['class' => 'btn btn-success active pull-center']); !!}
</div>

{!! Form::close() !!}

</form>
</div>
@endsection()

<script>
  $(document).ready(function(){
   $('form').parsley();
 });
</script>

