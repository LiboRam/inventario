@extends('templates.master')
@section('content')

<header class="panel-heading">
    <h2 class="panel-title">Actualizar datos del municipio</h2>
</header>
<div class="panel-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="mb-md">

                <a href="{{url ('municipios') }}" class="btn btn-primary mt-4 ml-3">Regresar
                </a>
            </div>
        </div>
    </div>

    {!! Form::open(['idMunicipio' => 'dataForm', 'method' => 'PATCH', 'url' => '/municipios/' . $municipio->idMunicipio, 'data-parsley-validate' ]) !!}
    <div class="col-sm-8 col-md-offset-2  col-xs-12">
        <div class="form-group">
            {!! Form::label('clave', 'Clave:'); !!}
            {!! Form::text('clave', $municipio->clave, ['placeholder' => 'Agregar clave', 'class' => 'form-control','required',
            'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
        </div>
    </div>
    <div class="col-sm-8 col-md-offset-2  col-xs-12">
        <div class="form-group">
            {!! Form::label('nombreMunicipio', 'Municipio:'); !!}
            {!! Form::text('nombreMunicipio', $municipio->nombreMunicipio, ['placeholder' => 'Nombre Municipio', 'class' => 'form-control','required',
            'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
        </div>
    </div>
    <div class="col-sm-8 col-md-offset-2  col-xs-12">
       <div class="form-group">
        {!! Form::label('region', 'Region:'); !!}
        {!! Form::text('region', $municipio->region, ['placeholder' => 'Nombre Region', 'class' => 'form-control','required',
        'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
    </div>
</div>

{!! Form::submit('Actualizar', ['class' => 'btn btn-primary pull-right']); !!}

{!! Form::close() !!}
</div>
@endsection()
<script>
  $(document).ready(function(){
   $('form').parsley();
});
</script>