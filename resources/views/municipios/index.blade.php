@extends('templates.master')

@section('content')
<header class="panel-heading">
  <span class="separator">
    <h2 class="panel-title">Listado de municipios</h2>
  </span>
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-10 col-md-offset-1  col-xs-12">
      <!--<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">-->
        <div class="mb-md">
          <a href="{{url ('municipios/create') }}" class="btn btn-primary mt-4 ml-3"> <i class="fa fa-plus">Agregar</i>  
          </a>
        </div>

        @if(Session::has('message'))
        <div class="alert alert-default alert-dismissible mt-5">
          <strong>{!! Session('message') !!}</strong>
          <button type="button" class="close" data-dismiss="alert">
            <span>x</span>
          </button>
        </div>
        @endif()

        <table class="table table-bordered table-hover mb-none" id="datatable-default">
         <thead>
          <tr>

            <th width="110px;">Clave</th>
            <th>Nombre Municipio</th>
            <th><?php  
            echo utf8_decode("Región");?></th>
            <th width="110px;"><?php  
            echo utf8_decode("Acción");?></th>
          </tr>
        </thead>
        <tbody>

          @foreach($municipios as $municipio)
          <tr>

            <td>{!! $municipio->clave !!}</td>
            <td>{!! $municipio->nombreMunicipio !!}</td>
            <td>{!! $municipio->region !!}</td>
            <td>
             {!! Form::open(['route'=>['municipios.destroy', $municipio->idMunicipio],'method'=>'DELETE', 'class' => 'form-horizontal','role' => 'form', 'onsubmit' => 'return ConfirmDelete()']) !!}                                            
             <a href="{{route('municipios.edit', $municipio->idMunicipio) }}" class="btn btn-default btn-sm" role="button"><i class="fa fa-pencil" aria-hidden="true"></i> </a> 

             <button type="submit" name="button" class="btn btn-default btn-sm">
              <i class="fa fa-trash-o"></i>
            </button>
            {!! Form::close() !!} 
            
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
</div>
<script type="text/javascript">
  function ConfirmDelete()
  {
    var x = confirm("Estas seguro de Eliminar?");
    if (x)
      return true;
    else
      return false;
  } 
</script>

@endsection()



