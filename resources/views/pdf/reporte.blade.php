@extends('templates.master')
@section('content')
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Reporte solicitantes (prueba)</title>

	<style type="text/css">
		table{
			font-family: arial, sans-serif;
		border-collapse: collapse;
		width: 100%;
		}

		td, th{
			border: 1px solid #dddddd;
			text-align: left;
			padding: 8px;
		}
		tr:nth-child(even){
			background-color: #dddddd;
		}
	</style>
</head>
<body>
<h2>Reporte de solicitantes</h2>
   <table class="table table-bordered table-hover mb-none" >
      <thead>
        <tr>
          <th style="padding-left: 15px;" width="110px;">Clave</th>
          <th>Nombre completo</th>
          <th>Cargo</th>
          <th>Dependencia</th>
          <th><?php  
          echo utf8_decode("Teléfono");?></th>
           <th width="80px;">
            Acciones
          </th>
      </thead>
      <tbody>

        @foreach($solicitantes as $solicitante)
        <tr>
          <td style="padding-left: 15px;">{!! $solicitante->idSolicitante !!}</td>
          <td>{!! $solicitante->name_complete !!}</td>
          <td>{!! $solicitante->cargo !!}</td>
          <td>{!! $solicitante->dependencia !!}</td>
          <td>{!! $solicitante->telefono !!}</td>
        </tr>
        @endforeach
      </tbody>
    </table>

</body>
</html>
@endsection()
