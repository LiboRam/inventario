@extends('templates.master')

@section('content')

<header class="panel-heading">
    <h2 class="panel-title">Insertar datos</h2>
</header>
<div class="panel-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="mb-md">

                <a href="{{url ('resguardo_equipos') }}" class="btn btn-primary mt-4 ml-3">Regresar  
                </a>
            </div>
        </div>
    </div>
    {!! Form::open(['resguardo_id' => 'dataForm', 'url' => '/resguardo_equipos','data-parsley-validate']) !!}
    <div class="col-sm-8 col-md-offset-2  col-xs-12">
        <div class="form-group">
            {!! Form::label('resguardo_id', 'Resguardo:'); !!}
            {!! Form::select('resguardo_id', $resguardos->pluck('nombreResguardo', 'idResguardo'), null, ['class' => 'form-control','required',
            'data-parsley-required-message' =>'Seleccione una lista']); !!}
        </div>
    </div>
    <div class="col-sm-8 col-md-offset-2  col-xs-12">
        <div class="form-group">
            {!! Form::label('cantidadEntregado', 'Cantidad de equipos entregados:'); !!}
            {!! Form::input('numeric', 'cantidadEntregado', null, ['class' => 'form-control','required',
            'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
        </div>
    </div>

    {!! Form::submit('Guardar', ['class' => 'btn btn-primary pull-right']); !!}

    {!! Form::close() !!}
</div>
@endsection()
<script>
  $(document).ready(function(){
   $('form').parsley();
});
</script>
