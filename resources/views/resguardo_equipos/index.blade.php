@extends('templates.master')

@section('content')

<header class="panel-heading">
  <span class="separator">
    <h2 class="panel-title">Listado de equipos entregados</h2>
  </span>
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-5">
      <div class="mb-md">
        <a href="{{url ('resguardo_equipos/create') }}" class="btn btn-primary mt-4 ml-3"> <i class="fa fa-plus">Agregar</i>  
        </a>
      </div>
    </div>
  </div>

  @if(Session::has('message'))
  <div class="alert alert-danger alert-dismissible mt-5">
    <strong>{!! Session('message') !!}</strong>
    <button type="button" class="close" data-dismiss="alert">
      <span>x</span>
    </button>
  </div>
  @endif()

  <table class="table table-bordered table-hover mb-none">
    <thead>
      <tr>
        <th style="padding-left: 15px;" width="110px;">Calve</th>
        <th>Cantidad de equipos entregados:</th>
        <th width="110px;">Accion</th>
      </tr>
    </thead>
    <tbody>

      @foreach($resguardo_equipos as $res)
      <tr>
        <td style="padding-left: 15px;">{!! $res->resguardo_id !!}</td>
        <td>{!! $res->cantidadEntregado !!}</td>
        <td>
          <a href="#" class="hidden on-editing save-row"><i class="fa fa-save"></i></a>
          <a href="#" class="hidden on-editing cancel-row"><i class="fa fa-times"></i></a>

          <a href="resguardo_equipos/{!! $res->resguardo_id !!}/edit" class="on-default edit-row"><i class="fa fa-pencil"></i></a>

          <a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a>

        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection()