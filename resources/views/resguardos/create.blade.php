@extends('templates.master')
@section('content')

<header class="panel-heading">
    <h2 class="panel-title">Insertar datos del resguardo</h2>
</header>
<div class="panel-body">
    <div class="row">
      <div class="col-sm-12">
        <div class="mb-md">

          <a href="{{url ('resguardos') }}" class="btn btn-primary mt-4 ml-3">Regresar  
          </a>
      </div>
  </div>
</div>

{!! Form::open(['idResguardo' => 'dataForm', 'url' => '/resguardos','data-parsley-validate']) !!}

<div class="col-sm-8 col-md-offset-2  col-xs-12">
    <div class="form-group">
        {!! Form::label('nombreResguardo', 'Resguardo:'); !!}
        {!! Form::text('nombreResguardo', null, ['placeholder' => 'resguardo', 'class' => 'form-control', 'required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
    </div>
</div>
<div class="col-sm-8 col-md-offset-2  col-xs-12">
    <div class="form-group">
        {!! Form::label('fechaInicio', 'Fecha de inicio:'); !!}
        {!! Form::input('date','fechaInicio', date('Y-m-d'), ['placeholder' => 'fecha de inicio', 'class' => 'form-control', 'required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
    </div>
</div>
<div class="col-sm-8 col-md-offset-2  col-xs-12">
    <div class="form-group">
        {!! Form::label('fechaFin', 'Fecha limite:'); !!}
        {!! Form::input('date','fechaFin', date('Y-m-d'), ['placeholder' => 'fecha limite', 'class' => 'form-control', 'required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
    </div>
</div>
<div class="col-sm-8 col-md-offset-2  col-xs-12">
    <div class="form-group">
        {!! Form::label('observaciones', 'Observaciones:'); !!}
        {!! Form::text('observaciones', null, ['placeholder' => 'observaciones', 'class' => 'form-control', 'required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
    </div>
</div>

<div class="col-sm-8 col-md-offset-2  col-xs-12 form-group">
    {!! Form::label('equipo_id', 'Nombre del equipo:'); !!}
    {!! Form::select('equipo_id', $equipo->pluck('nombreEquipo', 'idEquipo'), null, ['class' => 'form-control','placeholder' => '--selecciona--','required',
    'data-parsley-required-message' =>'Selecciona una lista']); !!}
</div>

<div class="column">
 <div class="col-sm-8 col-md-offset-2  col-xs-12 control-label form-group">
    {!! Form::label('solicitud_id', 'Nombre de la solicitud:'); !!}
    {!! Form::select('solicitud_id', $solicitud->pluck('nombreSolicitud', 'idSolicitud'), null, ['class' => 'form-control','placeholder' => 'Selecciona','required',
    'data-parsley-required-message' =>'Selecciona una lista']); !!}
</div>
</div>


<div class="column">
 <div class="col-sm-8 col-md-offset-2  col-xs-12 control-label form-group">
    {!! Form::label('ubicacion_id', 'Ubicacion del resguardo:'); !!}
    {!! Form::select('ubicacion_id', $ubicacion->pluck('nombreUbicacion', 'idUbicacion'), null, ['class' => 'form-control','placeholder' => 'Selecciona','required',
    'data-parsley-required-message' =>'Selecciona una lista']); !!}
</div>
</div>



</div>
<footer class="panel-footer">
    {!! Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary active')) !!} 
    {!! link_to(URL::previous(), 'Cancelar', ['class' => 'btn btn-default active',]) !!} 
</footer>
@endsection()