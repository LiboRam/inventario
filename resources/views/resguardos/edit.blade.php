@extends('templates.master')

@section('content')

<header class="panel-heading">
  <h2 class="panel-title">Actualizar datos del resguardo</h2>
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-12">
      <div class="mb-md">
        <a href="{{url ('resguardos') }}" class="btn btn-primary mt-4 ml-3">Regresar  
        </a>
      </div>
    </div>
  </div>
  {!! Form::open(['idResguardo' => 'dataForm', 'method' => 'PATCH', 'url' => '/resguardos/' . $resguardo->idResguardo,'data-parsley-validate']) !!}

  
  <div class="col-sm-8 col-md-offset-2  col-xs-12 control-label form-group">
    {!! Form::label('nombreResguardo', 'Resguardo:'); !!}
    {!! Form::text('nombreResguardo', $resguardo->nombreResguardo, ['placeholder' => 'Resguardo', 'class' => 'form-control','required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
  </div>


  <div class=" col-sm-8 col-md-offset-2  col-xs-12 control-label form-group">
    {!! Form::label('fechaInicio', 'Fecha de inicio:'); !!}
    {!! Form::text('fechaInicio', $resguardo->fechaInicio, ['placeholder' => 'Fecha de inicio', 'class' => 'form-control','required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
  </div>

  <div class=" col-sm-8 col-md-offset-2  col-xs-12 control-label form-group">
    {!! Form::label('fechaFin', 'Fecha limite:'); !!}
    {!! Form::text('fechaFin', $resguardo->fechaFin, ['placeholder' => 'Fecha limite', 'class' => 'form-control','required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
  </div>
  <div class="col-sm-8 col-md-offset-2  col-xs-12 control-label form-group">
    {!! Form::label('observaciones', 'observaciones:'); !!}
    {!! Form::text('observaciones', $resguardo->observaciones, ['placeholder' => 'observaciones', 'class' => 'form-control','required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
  </div>

  <div class=" col-sm-8 col-md-offset-2  col-xs-12 control-label form-group">
    <div class="form-group">
      {!! Form::label('solicitud_id', 'Solicitud:'); !!}
      {!! Form::select('solicitud_id', $solicitud->pluck('nombreSolicitud', 'idSolicitud'), null, ['class' => 'form-control','required','data-parsley-required-message' =>'Seleccione una lista']); !!}
    </div>
  </div>

  <div class=" col-sm-8 col-md-offset-2  col-xs-12 control-label form-group">
    {!! Form::label('ubicacion_id', 'ubicacion:'); !!}
    {!! Form::select('ubicacion_id', $ubicacion->pluck('nombreUbicacion', 'idUbicacion'), null, ['class' => 'form-control','required','data-parsley-required-message' =>'Seleccione una lista']); !!}
  </div>

</div>
<footer class="panel-footer">
  {!! Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary active')) !!} 
  {!! link_to(URL::previous(), 'Cancelar', ['class' => 'btn btn-default active',]) !!} 
</footer>
@endsection()

<script>
  $(document).ready(function(){
   $('form').parsley();
 });
</script>

