@extends('templates.master')

@section('content')

<header class="panel-heading">
  <span class="separator">
    <h2 class="panel-title">Listado de resguardos</h2>
  </span>
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-5">
      <div class="mb-md">
        <a href="{{url ('resguardos/create') }}" class="btn btn-primary mt-4 ml-3"> <i class="fa fa-plus">Agregar</i>  
        </a>
      </div>
    </div>
  </div>

  @if(Session::has('message'))
  <div class="alert alert-default alert-dismissible mt-5">
    <strong>{!! Session('message') !!}</strong>
    <button type="button" class="close" data-dismiss="alert">
      <span>x</span>
    </button>
  </div>
  @endif()
  <table class="table table-bordered table-hover table-striped mb-none" id="datatable-detailres">
    <thead>
      <tr>
        <th style="padding-left: 15px;" width="10px;">#</th>
        <th>Nombre del resguardo</th>
        <th class="hide">Fecha inicio</th>
        <th>Fecha limite</th>
        <th class="hide">Observaciones</th>
        <th class="hide">Nombre de la solicitud</th>
        
        <th>Nombre de la ubicacion</th>
        <th width="30px;">Acciones </th>
      </tr>
    </thead>
    <tbody>

      @foreach($resguardos as $resguardo)
      <tr class="actions">
        <td style="padding-left: 15px;">{!! $resguardo->idResguardo !!}</td>
        <td>{!! $resguardo->nombreResguardo !!}</td>
        <td class="hide">{!! $resguardo->fechaInicio !!}</td>
        <td>{!! $resguardo->fechaFin !!}</td>
        <td class="hide">{!! $resguardo->observaciones !!}</td>
        <td class="hide">{!! $resguardo->Solicitud->nombreSolicitud !!}</td>
        
        <td>{!! $resguardo->Ubicacion->nombreUbicacion !!}</td>
        <td class="actions">

         {!! Form::open(['route'=>['resguardos.destroy', $resguardo->idResguardo],'method'=>'DELETE', 'class' => 'form-horizontal','role' => 'form' , 'onsubmit' => 'return ConfirmDelete()']) !!} 

         <a href="resguardos/{!! $resguardo->idResguardo !!}/edit" class="btn btn-default btn-sm" title="Editar" role="button"><i class="fa fa-pencil"></i></a>
         <button type="submit" name="button" class="btn btn-default btn-sm" title="Eliminar">
          <i class="fa fa-trash-o"></i>
        </button>

        {!! Form::close() !!} 
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endsection()

<script type="text/javascript">
  function ConfirmDelete()
  {
    var x = confirm("Estas seguro de Eliminar?");
    if (x)
      return true;
    else
      return false;
  } 
</script>