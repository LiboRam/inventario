<!DOCTYPE html>
<html> 
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Resguardo</title>


  <table>
    <thead>
      <tr> 
       <td colspan="4"> <?php $image_path = '/images/imgInf.jpg';  ?>
       <img src="{{ public_path() . $image_path }}" style="width:700px; height:100px; align:right"></td>
     </tr>
     <tr>
      <td colspan="3"><strong>Nombre del resguardo</strong></td>
      <td rowspan="1" width="4%"><strong>Fecha impreso:</strong> </td>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <td colspan="4"><strong>Formato para el Resguardo de Equipo</strong></td>
    </tr>
  </tfoot>
</table>

<style type="text/css">

  /*table{
   font-family: arial, sans-serif;
   border-collapse: collapse;
   width: 100%;
   } */

   td, th{
     border: 1px solid black;
     text-align: left;
     padding: 8px;
   }
   th {
     background-color: #dddddd;
     text-align: center;
   }

  /* h2 {
     text-align: center;
     } */
     body {
      font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
      font-size: 14px;
      line-height: 1.42857143;
      color: #333333;
      background-color: #ffffff;
    }

    .footer {
     width: 100%;
     height: 200px;
     border-top: 1px solid #000;
     position: absolute;
     bottom: -5px;
   } 
   table{
     font-family: arial, sans-serif;
     border-collapse: collapse;
     width: 100%;
   }

   .izquierda {
    float:left;
    width: 43%;
  }
  .derecha {
    float:right;
    width: 43%;
  }


  .saltopagina{
    page-break-after:always;
  }

</style>
</head>
<body>
  <div clas='saltopagina'>

   <section>
     <article>
       <fieldset>
         <legend><strong>Datos del resguardante:</strong></legend>
         <br>
         <strong>Nombre del resguardante: <em> </em></strong><br>
         <strong>Dependencia:<em> </em></strong><br>
         <strong>Puesto:<em> </em></strong>
       </fieldset>
     </article> 
   </section><br>

   <table>
     <caption>Descripción del equipo</caption>
     <thead>
      <tr>
        <th style="padding-left: 15px;" width="5%;">#</th>
        <th>Nombre del equipo</th>
        <th>Marca</th>
        <th>Modelo</th>
        <th>Condicion</th>
      </tr>
    </thead>
    <!--<tbody>
      @foreach($solicitud as $solicituds)
      <tr>
        <td style="padding-left: 15px;" width="5%;">{!! $solicituds->idSolicitud !!}</td>
        <td>{!! $solicituds->Equipo->nombreEquipo !!}</td>
        <td>{!! $solicituds->Equipo->Marca->nombreMarca !!}</td>
        <td>{!! $solicituds->Equipo->Modelo->nombreModelo !!}</td>
        <td class="hide">{!! $solicituds->status !!}</td>
        <td class="hide">{!! $solicituds->Solicitante->dependencia !!}</td>
        <td class="hide">{!! $solicituds->Solicitante->cargo !!}</td>
        
      </tr>
      @endforeach    
    </tbody> -->
  </table>
</div>
<br>
<aside>
  <fieldset>
    <legend><strong>Disposiciones para el uso y manejo del equipo:</strong></legend>
    <ol>
      <li>El bien resguardado será de uso personal e instransferible y será utilizado exclusivamente para el desempeño de sus actividades propias en materia de seguridad</li>
      <li>
        El usuario se compromete a dar el uso debido al bien descrito, evitando causar daños o deterioro al mismo.
      </li>
      <li>
        El usuario no podrá abrir ni modificar los componentes del equipo descrito y cuando sea necesario el mantenimiento preventivo o correctivo del mismo, deberá informarlo oportunamente al areá responsable de la Dirección General del Centro de
        Control Comando y Comunicación
      </li>
      <li>
        El usuario del equipo asigando deberá proporcionar a la oficina de control una clave de identificación que usará para comunicarse.
      </li>
      <li>
        El usuario se compromete a manejar con reserva la información que por virtud del uso del equipo que se le asigna, tenga conocimiento.
      </li>
      <li>
        El usuario se obliga a responder por el robo o extravio del equipo descrito, cubriendo el valor del mismo en los términos del convenio que al efecto celebre con la Dirección General del Centro de Control Comando y Comunicación.
      </li>
      <li>
        Se compromete a usar personalmente el equipo descrito y presentarlo en el momento en el que le sea requerido por el areá responsable de la Dirección General del Centro de
        Control Comando y Comunicación.
      </li>

      <li>
        En caso de pérdida o robo, el usuario se compromete a pagar la cantidad correspondiente al(los) modelo(s) del(los) equipo(s) que le ha sido entregado(s).
      </li>
    </ol>
  </aside>
  <br>
  <section>
    <form>
     <fieldset class="izquierda">
       <legend><strong>AUTORIZÓ</strong></legend><br><br><br><br>

       ______________________________________
       <P>Ing. Jesus Ivan Salomon Garcia.
       Dir. Gral. del Centro de Control Comando y Comunicación</P>
     </fieldset>

     <fieldset class="derecha">
       <legend><strong>RECIBE DE CONFORMIDAD</strong></legend>
       <small>Nombre:</small><br>
       <small>Cargo:</small><br><br><br>
       ____________________________________
       <p>Firma</p>

     </fieldset>
   </form>
 </section>

</body>
<br>
<footer class="footer">
  <br>
  <p>Prolongación de Xiconténcatl 1031, Col. Eliseo Jiménez Ruiz, Oaxaca de Juárez, Oaxaca, C.P.68120 Tel. (951) 50 1 50 45 Ext. 10240</p>
</footer>
</html>