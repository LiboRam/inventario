@extends('templates.master')
@section('content')

<header class="panel-heading">
  <h2 class="panel-title">Insertar datos del solicitante</h2>
</header>
<div class="panel-body">
  <div class="row">
    <div class="col-sm-12">
      <div class="mb-md">

        <a href="{{url ('solicitantes') }}" class="btn btn-primary" style="margin-bottom: 15px;">Regresar  
        </a>
      </div>
    </div>
  </div>

  {!! Form::open(['idSolicitante' => 'dataForm', 'url' => '/solicitantes','data-parsley-validate']) !!}

  
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="form-group">
      {!! Form::label('nombreSolicitante', 'Nombre:'); !!}
      {!! Form::text('nombreSolicitante', null, ['placeholder' => 'Nombre Solicitante', 'class' => 'form-control' ,'required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
    </div>
  </div>
  

  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="form-group">
      {!! Form::label('apellidoP','Apellido Paterno:');  !!}
      {!! Form::text('apellidoP', null, ['placeholder' => 'Apellido Paterno', 'class' => 'form-control','required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
    </div>
  </div>


  
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
   <div class="form-group">
    {!! Form::label('apellidoM', 'Apellido Materno:'); !!}
    {!! Form::text('apellidoM', null, ['placeholder' => 'Apellido Materno', 'class' => 'form-control','required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
  </div>
</div>



<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
  <div class="form-group">
    {!! Form::label('cargo', 'Cargo:'); !!}
    {!! Form::text('cargo', null, ['placeholder' => 'Cargo solicitante', 'class' => 'form-control','required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
  </div>
</div>



<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
  <div class="form-group">
    {!! Form::label('dependencia', 'Dependencia:'); !!}
    {!! Form::text('dependencia', null, ['placeholder' => 'Nombre Dependencia', 'class' => 'form-control','required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
  </div>
</div>



<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
 <div class="form-group">
  {!! Form::label('telefono', 'Número del Teléfono:'); !!}
  {!! Form::text('telefono', null, ['placeholder' => 'Numero tel', 'class' => 'form-control','required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
</div>
</div>
</div>
<footer class="panel-footer">
  {!! Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary active')) !!} 
  {!! link_to(URL::previous(), 'Cancelar', ['class' => 'btn btn-default active',]) !!} 
</footer>
@endsection()

<script>
  $(document).ready(function(){
   $('form').parsley();
 });
</script>