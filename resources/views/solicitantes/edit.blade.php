@extends('templates.master')
@section('content')

<header class="panel-heading">
    <h2 class="panel-title">Actualizar datos del solicitante</h2>
</header>
<div class="panel-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="mb-md">

                <a href="{{url ('solicitantes') }}" class="btn btn-primary mt-4 ml-3">Regresar 
                </a>
            </div>
        </div>
    </div>

    {!! Form::open(['idSolicitante' => 'dataForm', 'method' => 'PATCH', 'url' => '/solicitantes/' . $solicitante->idSolicitante ]) !!}
    <div class="column">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                {!! Form::label('nombreSolicitante', 'Nombre:'); !!}
                {!! Form::text('nombreSolicitante', $solicitante->nombreSolicitante, ['placeholder' => 'Nombre Solicitante', 'class' => 'form-control']); !!}
            </div>
        </div>
    </div>

    <div class="column">
        <div class="clearfix">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    {!! Form::label('apellidoP', 'Apellido Paterno:'); !!}
                    {!! Form::text('apellidoP', $solicitante->apellidoP, ['placeholder' => 'Apellido Paterno', 'class' => 'form-control']); !!}
                </div>
            </div>
        </div>

        <div class="column">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
             <div class="form-group">
                {!! Form::label('apellidoM', 'Apellido Materno:'); !!}
                {!! Form::text('apellidoM', $solicitante->apellidoM, ['placeholder' => 'Apellido Materno', 'class' => 'form-control']); !!}
            </div>
        </div>
    </div>
    <div class="column">
        <div class="clearfix">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    {!! Form::label('cargo', 'Cargo:'); !!}
                    {!! Form::text('cargo', $solicitante->cargo, ['placeholder' => 'Cargo solicitante', 'class' => 'form-control']); !!}
                </div>
            </div>
        </div>
    </div>
    
    <div class="column">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                {!! Form::label('dependencia', 'Dependencia:'); !!}
                {!! Form::text('dependencia', $solicitante->dependencia, ['placeholder' => 'Nombre Dependencia', 'class' => 'form-control']); !!}
            </div>
        </div>
    </div>

    <div class="column">
        <div class="clearfix">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
             <div class="form-group">
                {!! Form::label('telefono', 'Número del Teléfono:'); !!}
                {!! Form::text('telefono', $solicitante->telefono, ['placeholder' => 'Numero tel', 'class' => 'form-control']); !!}
            </div>
        </div>
    </div>
</div>
</div>
</div>
<footer class="panel-footer">
    {!! Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary active')) !!} 
    {!! link_to(URL::previous(), 'Cancelar', ['class' => 'btn btn-default active',]) !!} 
</footer>
@endsection()