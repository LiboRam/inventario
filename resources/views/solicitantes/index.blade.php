@extends('templates.master')
@section('content')
<header class="panel-heading">
  <span >
    <h2 class="panel-title">Listado de solicitantes</h2>

  </span>
</header>
<div class="panel-body">
  <div class="row">
   <div class="col-lg-12 col-md-10 col-sm-10 col-xs-12">
    <div class="mb-md">
      <a href="{{url ('solicitantes/create') }}" class="btn btn-primary mt-4 ml-3" style="margin-bottom: 15px;"> <i class="fa fa-plus">Agregar</i>  
      </a>
      <a href="{{ route('downloadScte') }}" class="btn btn-default mt-4 ml-3 pull-right" title="Descargar PDF"> <i class="fa fa-file-pdf-o" >PDF</i>  
      </a>
    </div>
  </div>
</div>

@if(Session::has('message'))
<div class="alert alert-default alert-dismissible mt-5">
  <strong>{!! Session('message') !!}</strong>
  <button type="button" class="close" data-dismiss="alert">
    <span>x</span>
  </button>
</div>
@endif()
<table class="table table-bordered table-hover mb-none" id="datatable-default">
  <thead>
    <tr>
      <th style="padding-left: 15px;" width="110px;">Clave</th>
      <th>Nombre completo</th>
      <th>Puesto</th>
      <th>Dependencia</th>
      <th><?php  
      echo utf8_decode("Teléfono");?></th>
      <th width="80px;">
        Acciones
      </th>
    </thead>
    <tbody>

      @foreach($solicitantes as $solicitante)
      <tr>
        <td style="padding-left: 15px;">{!! $solicitante->idSolicitante !!}</td>
        <td>{!! $solicitante->name_complete !!}</td>
        <td>{!! $solicitante->cargo !!}</td>
        <td>{!! $solicitante->dependencia !!}</td>
        <td>{!! $solicitante->telefono !!}</td>

        <td class="actions" >

          {!! Form::open(['route'=>['solicitantes.destroy', $solicitante->idSolicitante],'method'=>'DELETE', 'class' => 'form-horizontal','role' => 'form' , 'onsubmit' => 'return ConfirmDelete()']) !!} 

          <a href="solicitantes/{!! $solicitante->idSolicitante !!}/edit" class="btn btn-default btn-sm" title="Editar" role="button"><i class="fa fa-pencil"></i></a>
          <button type="submit" name="button" class="btn btn-default btn-sm" title="Eliminar">
            <i class="fa fa-trash-o"></i>
          </button>

          {!! Form::close() !!} 
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

<script type="text/javascript">
  function ConfirmDelete()
  {
    var x = confirm("Estas seguro de Eliminar?");
    if (x)
      return true;
    else
      return false;
  } 
</script>

@endsection()