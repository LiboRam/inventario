<!DOCTYPE html>
<html> 
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Informe de solicitantes</title>


  <table>
    <thead>
      <tr> 
       <td colspan="4"> <?php $image_path = '/images/imgInf.jpg';  ?>
         <img src="{{ public_path() . $image_path }}" style="width:700px; height:100px; align:right"></td>
       </tr>
       <tr>
        <td colspan="3"><strong>Lista de los solicitantes</strong></td>
        <td rowspan="1" width="4%"><strong>Fecha impreso: {{$today}}</strong></td>
      </tr>: 
    </thead>
  </table>

  <style type="text/css">

  /*table{
   font-family: arial, sans-serif;
   border-collapse: collapse;
   width: 100%;
   } */

   td, th{
     border: 1px solid black;
     text-align: left;
     padding: 8px;
   }
   th {
     background-color: #dddddd;
     text-align: center;
   }

  /* h2 {
     text-align: center;
     } */
     body {
      font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
      font-size: 14px;
      line-height: 1.42857143;
      color: #333333;
      background-color: #ffffff;
    }

    .footer {
     width: 100%;
     height: 200px;
     border-top: 1px solid #000;
     position: absolute;
     bottom: -5px;
   } 
   table{
     font-family: arial, sans-serif;
     border-collapse: collapse;
     width: 100%;
   }

   .izquierda {
    float:left;
    width: 43%;
  }
  .derecha {
    float:right;
    width: 43%;
  }


  .saltopagina{
    page-break-after:always;
  }

</style>
</head>
<body>
  <br>
  <div clas='saltopagina'>
   <table>
    <thead>
      <tr>
        <th style="padding-left: 15px;" width="5%;">#</th>
        <th>Nombre completo</th>
        <th>Puesto</th>
        <th>Dependencia</th>
        <th>Teléfono</th>
      </tr>
    </thead>
    <tbody>
      @foreach($solicitantes as $solicitante)
      <tr>
        <td style="padding-left: 8px;">{{ $solicitante->idSolicitante }}</td>
        <td>{{ $solicitante->name_complete }}</td>
        <td>{{ $solicitante->cargo }}</td>
        <td>{{ $solicitante->dependencia  }}</td>
        <td>{{ $solicitante->telefono  }}</td>
      </tr>
      @endforeach    
    </tbody>
  </table>
</div>
</body>
<br>
<footer class="footer">
  <br>
  <p>Prolongación de Xiconténcatl 1031, Col. Eliseo Jiménez Ruiz, Oaxaca de Juárez, Oaxaca, C.P.68120 Tel. (951) 50 1 50 45 Ext. 10240</p>
</footer>
</html>