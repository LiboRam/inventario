@extends('templates.master')

@section('content')
<header class="panel-heading">
    <h2 class="panel-title">Insertar datos de la solicitud</h2>
</header>
<div class="panel-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="mb-md">

                <a href="{{url ('solicituds') }}" class="btn btn-primary" style="margin-bottom: 15px;">Regresar  
                </a>
            </div>
        </div>
    </div>

    {!! Form::open(['idSolicitud' => 'dataForm', 'url' => '/solicituds','data-parsley-validate']) !!}
    <div class="col-sm-8 col-md-offset-2  col-xs-12 form-group">
        {!! Form::label('nombreSolicitud', 'Nombre de la solicitud:'); !!}
        {!! Form::text('nombreSolicitud', null, ['placeholder' => 'Solicitud', 'class' => 'form-control','required',
        'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
    </div>
    <!--<div class="col-sm-8 col-md-offset-2  col-xs-12 form-group">
        {!! Form::label('descripcion', 'Descripcion:'); !!}
        {!! Form::text('descripcion', null, ['placeholder' => 'descripcion de la solicitud', 'class' => 'form-control','required',
        'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
    </div> -->

    <div class="col-sm-8 col-md-offset-2  col-xs-12 form-group">
        {!! Form::label('fechaSolicitud', 'Fecha de solicitud:'); !!}
        {!! Form::input('date','fechaSolicitud', date('Y-m-d'), ['placeholder' => 'fecha de la solicitud', 'class' => 'form-control','required',
        'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
    </div>
    <div class="col-sm-8 col-md-offset-2  col-xs-12 form-group">
        {!! Form::label('fechaEntrega', 'Fecha de entrega:'); !!}
        {!! Form::input('date','fechaEntrega', date('Y-m-d'), ['placeholder' => 'fecha de entrega', 'class' => 'form-control','required',
        'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
    </div>
    <div class="col-sm-8 col-md-offset-2  col-xs-12 form-group">
        {!! Form::label('status', 'Estado de la solicitud:'); !!}
        {!! Form::text('status', null, ['placeholder' => 'Estado la solicitud', 'class' => 'form-control','required',
        'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
    </div>

    <div class="col-sm-8 col-md-offset-2  col-xs-12 form-group">
        {!! Form::label('solicitante_id', 'Nombre del solicitante:'); !!}
        {!! Form::select('solicitante_id', $solicitantes->pluck('nombreSolicitante', 'idSolicitante'), null, ['placeholder' => 'Seleccionar','class' => 'form-control','required',
        'data-parsley-required-message' =>'Seleccione una lista']); !!}
    </div>

    <div class="col-sm-8 col-md-offset-2  col-xs-12  form-group" >
        {!! Form::label('equipo_id', 'Nombre del equipo:'); !!}
        {!! Form::select('equipo_id', $equipo->pluck('nombreEquipo', 'idEquipo'), null, ['placeholder' => 'Seleccionar','class' => 'form-control','required',
        'data-parsley-required-message' =>'Seleccione una lista']); !!}
    </div>

</div>
<footer class="panel-footer">
  {!! Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary active')) !!} 
  {!! link_to(URL::previous(), 'Cancelar', ['class' => 'btn btn-default active',]) !!} 
</footer>
@endsection()

<script>
  $(document).ready(function(){
   $('form').parsley();
});
</script>


