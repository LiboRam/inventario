@extends('templates.master')

@section('content')
<header class="panel-heading">
    <h2 class="panel-title">Actualizar datos de la solicitud</h2>
</header>
<div class="panel-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="mb-md">
                <a href="{{url ('solicituds') }}" class="btn btn-primary mt-4 ml-3">Regresar 
                </a>
            </div>
        </div>
    </div>

    {!! Form::open(['idSolicitud' => 'dataForm', 'method' => 'PATCH', 'url' => '/solicituds/' . $solicitud->idSolicitud,'data-parsley-validate']) !!}

    <div class="col-sm-8 col-md-offset-2  col-xs-12">
        <div class="form-group">
            {!! Form::label('nombreSolicitud', 'Solicitud:'); !!}
            {!! Form::text('nombreSolicitud', $solicitud->nombreSolicitud, ['placeholder' => 'Solicitud', 'class' => 'form-control','required',
            'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
        </div>
    </div>
    <!--<div class="col-sm-8 col-md-offset-2  col-xs-12">
        <div class="form-group">
            {!! Form::label('descripcion', 'descripcion:'); !!}
            {!! Form::text('descripcion', $solicitud->descripcion, ['placeholder' => 'descripcion', 'class' => 'form-control','required',
            'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
        </div>
    </div> -->
    <div class="col-sm-8 col-md-offset-2  col-xs-12">
        <div class="form-group">
            {!! Form::label('fechaSolicitud', 'Fecha de solicitud:'); !!}
            {!! Form::text('fechaSolicitud', $solicitud->fechaSolicitud, ['placeholder' => 'Fecha de solicitud', 'class' => 'form-control','required',
            'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
        </div>
    </div>
    <div class="col-sm-8 col-md-offset-2  col-xs-12">
     <div class="form-group">
        {!! Form::label('fechaEntrega', 'Fecha de entrega:'); !!}
        {!! Form::text('fechaCreacion', $solicitud->fechaEntrega, ['placeholder' => 'Fecha de entrega', 'class' => 'form-control']); !!}
    </div>
</div>
<div class="col-sm-8 col-md-offset-2  col-xs-12">
 <div class="form-group">
    {!! Form::label('status', 'Estado:'); !!}
    {!! Form::text('status', $solicitud->status, ['placeholder' => 'Estado de la solicitud', 'class' => 'form-control','required',
    'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
</div>
</div>


<div class="col-sm-8 col-md-offset-2  col-xs-12">
    <div class="form-group">
        {!! Form::label('solicitante_id', 'Solicitante:'); !!}
        {!! Form::select('solicitante_id', $solicitante->pluck('nombreSolicitante', 'idSolicitante'), null, ['class' => 'form-control']); !!}
    </div>
</div>

<div class="col-sm-8 col-md-offset-2  col-xs-12 form-group">
    {!! Form::label('equipo_id', 'Equipo:'); !!}
    {!! Form::select('equipo_id', $equipo->pluck('nombreEquipo', 'idEquipo'), null, ['class' => 'form-control']); !!}
</div>

</div>
<footer class="panel-footer">
  {!! Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary active')) !!} 
  {!! link_to(URL::previous(), 'Cancelar', ['class' => 'btn btn-default active',]) !!} 
</footer>
@endsection()
<script>
  $(document).ready(function(){
     $('form').parsley();
 });
</script>