<!DOCTYPE html>
<html> 
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>Formato Solicitud</title>


  <table>
    <thead>
      <tr> 
       <td colspan="4"> <?php $image_path = '/images/imgInf.jpg';  ?>
       <img src="{{ public_path() . $image_path }}" style="width:700px; height:100px; align:right"></td>
     </tr>
     <tr>
      <td colspan="3"><strong>Resguardo de  {!! $solicitud[0]->nombreSolicitud !!}Z</strong></td>
      <td rowspan="1" width="4%"><strong>Fecha impreso:</strong>{{$today}}</td>
    </tr>
  </thead>
  <tfoot>
    <tr>
      <td colspan="4"><strong>Formato para el Resguardo de Equipo</strong></td>
    </tr>
  </tfoot>
</table>

<style type="text/css">

  /*table{
   font-family: arial, sans-serif;
   border-collapse: collapse;
   width: 100%;
   } */

   td, th{
     border: 1px solid black;
     text-align: left;
     padding: 8px;
   }
   th {
     background-color: #dddddd;
     text-align: center;
   }

  /* h2 {
     text-align: center;
     } */
     body {
      font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
      font-size: 14px;
      line-height: 1.42857143;
      color: #333333;
      background-color: #ffffff;
    }

    .footer {
     width: 100%;
     height: 200px;
     border-top: 1px solid #000;
     position: absolute;
     bottom: -5px;
   } 
   table{
     font-family: arial, sans-serif;
     border-collapse: collapse;
     width: 100%;
   }

   .izquierda {
    float:left;
    width: 43%;
  }
  .derecha {
    float:right;
    width: 43%;
  }


  .saltopagina{
    page-break-after:always;
  }

</style>
</head>
<body>
  <div clas='saltopagina'>

   <section>
     <article>
       <fieldset>
         <legend><strong>Datos del resguardante:</strong></legend>
         <br>
         <strong>Nombre del resguardante: <em> </em></strong><br>
         <strong>Dependencia:<em> </em></strong><br>
         <strong>Puesto:<em> </em></strong>
       </fieldset>
     </article> 
   </section><br>
   <!--
intente lo siguiente:

{{ isset($blogs[0]) ? $blogs[0]->title : '' }} -->

<table>
 <caption>Datos del equipo que se solicita</caption>
 <thead>
  <tr>
    <th style="padding-left: 15px;" width="5%;">#</th>
    <th>Nombre del equipo</th>
        <!--
        <th>Descripción</th>
        <th>Cantidad</th>
      -->
      
    </tr>
  </thead>
  <tbody>
    @foreach($solicitud as $solicituds)
    <tr>
      <td style="padding-left: 15px;" width="5%;">{!! $solicituds->idSolicitud !!}</td>
      <td>{!! $solicituds->Equipo->nombreEquipo !!}</td>
       <!-- <td>{!! $solicituds->Equipo->Marca->nombreMarca !!}</td>
        <td>{!! $solicituds->Equipo->Modelo->nombreModelo !!}</td>
        <td class="hide">{!! $solicituds->status !!}</td>
        <td class="hide">{!! $solicituds->Solicitante->cargo !!}</td> -->
        
      </tr>
      @endforeach    
    </tbody>
  </table>
</div>
<br>
<strong>Fecha para la que se necesita: <em>  08-08-2019</em></strong>
<br> <br>
<section>
  <form>
   <fieldset class="izquierda">
     <legend><strong>AUTORIZÓ</strong></legend><br><br><br><br>

     ______________________________________
     <small>Sello y firma del solicitante</small>
   </fieldset>

   <fieldset class="derecha">
     <legend><strong>RECIBE DE CONFORMIDAD</strong></legend><br><br><br>
     ____________________________________
     <small>Nombre completo y firma</small>

   </fieldset>
 </form>
</section>

</body>
</html>