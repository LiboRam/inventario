@extends('templates.master')

@section('content')
<!-- start: page -->
<header class="panel-heading">
  <span class="separator">
    <h2 class="panel-title">Listado de solicitudes</h2>
  </span>
</header>
<div class="panel-body">
  <div class="row">
   <div class="col-lg-12 col-md-10 col-sm-10 col-xs-12">
     <div class="mb-md">
      <a href="{{url ('solicituds/create') }}" class="btn btn-primary mt-4 ml-3"> <i class="fa fa-plus">Agregar</i>  </a>
    </div>
  </div>
</div>

@if(Session::has('message'))
<div class="alert alert-default alert-dismissible mt-5">
  <strong>{!! Session('message') !!}</strong>
  <button type="button" class="close" data-dismiss="alert">
    <span>x</span>
  </button>
</div>
@endif()

<table class="table table-bordered table-hover table-striped mb-none" id="datatable-details1">
  <thead>
    <tr>
      <th style="padding-left: 15px;" width="5%;">#</th>
      <th>Nombre de la solicitud</th>
      <th class="hide">Fecha solicitada</th>
      <th class="hide">Fecha a entregar</th>
      <th class="hide">Estatus de la solicitud</th>
      <th>Encargado</th>
      <th class="hide">Nombre del equipo</th>
      <!-- <th class="hide">user</th> -->

      <th width="15%;"> Acciones</th>
    </tr>
  </thead>
  <tbody>

    @foreach($solicituds as $solicitud)
    <tr>
      <td style="padding-left: 15px;" width="5%;">{!! $solicitud->idSolicitud !!}</td>
      <td>{!! $solicitud->nombreSolicitud !!}</td>
      <td class="hide">{!! $solicitud->fechaSolicitud !!}</td>
      <td class="hide">{!! $solicitud->fechaEntrega !!}</td>
      <td class="hide">{!! $solicitud->status !!}</td>
      <td>{!! $solicitud->Solicitante->name_complete !!}</td>
      <td class="hide">{!! $solicitud->Equipo->nombreEquipo !!}</td>
      <!-- <td class="hide">{!! $solicitud->Usuario->name_complete !!}</td>-->
      <td width="15%;" class="actions">
        {!! Form::open(['route'=>['solicituds.destroy', $solicitud->idSolicitud],'method'=>'DELETE', 'class' => 'form-horizontal','role' => 'form' , 'onsubmit' => 'return ConfirmDelete()']) !!} 
        <a href="{{ route('generateForm') }}" class="btn btn-default btn-sm" title="Descargar formato de la solicitud" role="button"> <i class="fa fa-file-pdf-o"></i>  
        </a>
        <a href="solicituds/{!! $solicitud->idSolicitud !!}/edit" class="btn btn-default btn-sm" title="Editar" role="button"><i class="fa fa-pencil"></i></a> 

        <button type="submit" name="button" class="btn btn-default btn-sm" title="Eliminar">
          <i class="fa fa-trash-o"></i>
        </button>
        {!! Form::close() !!} 
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>

<script type="text/javascript">
  function ConfirmDelete()
  {
    var x = confirm("Estas seguro de Eliminar?");
    if (x)
      return true;
    else
      return false;
  } 
</script>

@endsection()
