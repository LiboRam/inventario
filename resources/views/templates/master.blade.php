<!DOCTYPE html>
<html lang="es" class="fixed">
<head>

	<!-- Basic -->
	<meta charset="UTF-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Sistema de inventario</title>
	<meta name="keywords" content="HTML5 Admin Template" />
	<meta name="description" content="Responsive HTML5 Template">
	<meta name="author" content="okler.net">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<!-- Web Fonts  -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

	<!-- Vendor CSS -->
	<link rel="stylesheet" href="{{asset('assets/vendor/bootstrap/css/bootstrap.css')}}" />
	<link rel="stylesheet" href="{{asset('assets/vendor/font-awesome/css/font-awesome.css')}}" />
	<link rel="stylesheet" href="{{asset('assets/vendor/magnific-popup/magnific-popup.css')}}" />
	<link rel="stylesheet" href="{{asset('assets/vendor/bootstrap-datepicker/css/datepicker3.css')}}" />
	<link rel="stylesheet" href="{{asset('assets/vendor/parsley.css')}}" />

	<!-- Specific Page Vendor CSS -->
	<link rel="stylesheet" href="{{asset('assets/vendor/select2/select2.css')}}" />
	<link rel="stylesheet" href="{{asset('assets/vendor/jquery-datatables-bs3/assets/css/datatables.css')}}" />

	<!-- Theme CSS -->
	<link rel="stylesheet" href="{{asset('assets/stylesheets/theme.css')}}" />
	<link rel="stylesheet" href="{{asset('sweetAlert/sweetalert2.min.css')}}" />

	<!-- Skin CSS -->
	<link rel="stylesheet" href="{{asset('assets/stylesheets/skins/default.css')}}" />

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="{{asset('assets/stylesheets/theme-custom.css')}}">

	<!-- Head Libs -->
	<script src="{{asset('assets/vendor/modernizr/modernizr.js')}}"></script>

</head>
<body>
	<section class="body">
		<!-- start: header -->
		<header class="header" >
			<div class="container">
				<a href="../" class="logo">
					<img  src="{{asset('assets/images/SSPO.png')}}"  height="85" alt="Porto Admin" />
				</a>
				
				<div>
					<h2 class="titulo1">Sistema de Inventario <br> <small class="titulo2" >SECRETARIA DE SEGURIDAD PUBLICA</small></h2>
				</div>
				<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
					<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
				</div>
			</div> 



			<!-- start: search & user box -->
			<div class="header-right">
				<div id="userbox" class="userbox">
					<a href="#" data-toggle="dropdown">
						<figure class="profile-picture">
							<img src="{{asset('assets/images/adm.png')}}" alt="usuario" class="img-circle" data-lock-picture="{{asset('assets/images/adm.png')}}" />
						</figure>
						<div class="profile-info">
							<span class="name">Liborio</span>
							<span class="role">administrador</span>
						</div>

						<i class="fa custom-caret"></i>
					</a>

					<div class="dropdown-menu">
						<ul class="list-unstyled">
							<li class="divider"></li>
							<li>
								<a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> Bloquear pantalla</a>
							</li>
							<li>
								<a role="menuitem" tabindex="-1" href="login"><i class="fa fa-power-off"></i>Cerrar sesion</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<!-- end: search & user box -->
		</header>
		<!-- end: header -->


		<div class="inner-wrapper">
			<!-- start: sidebar -->
			<aside id="sidebar-left" class="sidebar-left">

				<div class="sidebar-header">
					<div class="sidebar-title">
						Menu
					</div>
					<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>

				<div class="nano">
					<div class="nano-content">
						<nav id="menu" class="nav-main" role="navigation">
							<ul class="nav nav-main">
								<li class="nav-active">
									<a href="javascript:void(0);" class="waves-effect">
										<i class="fa fa-home" aria-hidden="true"></i>
										<span>Inicio</span>
									</a>
								</li>

								
								<li class="nav-parent">
									<a href="javascript:void(0);" class="waves-effect">
										<i class="fa fa-laptop" aria-hidden="true" class="zmdi zmdi-local-shipping"></i>
										<span>Equipo</span><span class="menu-arrow"></span>
									</a>

									<ul class="nav nav-children">
										<li>
											<a href="{{url ('equipos') }}">Registros</a>
										</li>
									</ul>

									<ul class="nav nav-children">
										<li>
											<a href="{{url ('equipos/create') }}">Nuevo equipo</a>
										</li>
									</ul>
									<ul class="nav nav-children">
										<li >
											<a href="{{url ('stock-equipos') }}">Stock del equipo</a>
										</li>
									</ul>

									<ul class="nav nav-children ">
										<li class="nav-parent nav-active">
											<a> Caracteristicas</a>
											<ul class="nav nav-children"> 
												<li>
													<a href="{{url ('marcas') }}">
														Marca
													</a>
												</li>
												<li>
													<a href="{{url ('modelos') }}">
														Modelo
													</a>
												</li>

												<li>
													<a href="{{url ('categorias') }}">
														Categoria
													</a>
												</li>
												<li>
													<a href="{{url ('unidads') }}">
														Unidad
													</a>
												</li>
												<li>
													<a href="{{url ('edoequipos') }}">
														Estado equipo
													</a>
												</li>
											</ul>
										</li>
									</ul>
								</li>


								<li class="nav-parent">
									<a href="javascript:void(0);" class="waves-effect">
										<i class="fa fa-map-marker" aria-hidden="true" class="zmdi zmdi-local-shipping"></i>
										<span>Ubicacion</span><span class="menu-arrow"></span>
									</a>
									<ul class="nav nav-children">
										<li>
											<a href="{{url ('ubicacions') }}">Registros</a>
										</li>
									</ul>
									<ul class="nav nav-children">
										<li>
											<a href="{{url ('ubicacions/create') }}">Nueva ubicacion</a>
										</li>
									</ul>
									<ul class="nav nav-children">
										<li>
											<a href="{{url ('municipios') }}">
											Municipio</a>
										</li>
									</ul>
								</li>

								<li class="nav-parent">
									<a href="javascript:void(0);" class="waves-effect">
										<i class="fa fa-folder zmdi zmdi-local-shipping"  aria-hidden="true"></i>
										<span>Resguardo</span><span class="menu-arrow"></span>
									</a>

									<ul class="nav nav-children">
										<li>
											<a href="{{url ('resguardos') }}">Nuevo resguardo</a>
										</li>
									</ul>
									<ul class="nav nav-children">
										<li>
											<a href="{{url ('resguardo_equipos') }}"> Caracteristicas</a>
										</li>
									</ul>
								</li>

								<li class="nav-parent">
									<a href="javascript:void(0);" class="waves-effect">
										<i class="fa fa-user zmdi zmdi-local-shipping"  aria-hidden="true"></i>
										<span>Usuarios</span>
									</a>


									<ul class="nav nav-children">
										<li>
											<a href="{{url ('register') }}">Agregar usuario</a>
										</li>
									</ul>

									<ul class="nav nav-children">
										<li>
											<a href="{{url ('areas') }}">
											Areas</a>
										</li>
									</ul>
								</li>

								<li class="nav-parent">
									<a href="javascript:void(0);" class="waves-effect">
										<i class="fa fa-group zmdi zmdi-local-shipping" aria-hidden="true"></i>
										<span>Solicitantes</span>
									</a>
									
									<ul class="nav nav-children">
										<li>
											<a href="{{url ('solicitantes') }}">Registros</a>
										</li>
										<li>
											<a href="{{url ('solicitantes/create') }}">Nuevo solicitante</a>
										</li>
									</ul>
								</li>

								<li class="nav-parent">
									<a href="javascript:void(0);" class="waves-effect">
										<i class="fa fa-file-text zmdi zmdi-local-shipping" aria-hidden="true"></i>
										<span>Solicitud</span>
									</a>

									<ul class="nav nav-children">
										<li>
											<a href="{{url ('solicituds') }}">Registros</a>
										</li>
									</ul>
									<ul class="nav nav-children">
										<li>
											<a href="{{url ('solicituds/create') }}">Nueva solicitud</a>
										</li>
									</ul>
								</li>
							</ul>
						</nav>
						<hr class="separator" />
					</div>
				</div>
			</aside>
			<!-- end: sidebar -->

			<section role="main" class="content-body">

				<!-- start: page ******************************************************* -->
				<section class="panel">

					@yield('content')

				</section> 
				<!--- end: page***********************************************************************++ ---->	
			</section>
		</div>

	</section>

	<!-- Vendor -->
	<script src="{{asset('assets/vendor/jquery/jquery.js')}}"></script>
	<script src="{{asset('assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js')}}"></script>
	<script src="{{asset('assets/vendor/bootstrap/js/bootstrap.js')}}"></script>
	<script src="{{asset('assets/vendor/nanoscroller/nanoscroller.js')}}"></script>
	<script src="{{asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
	<script src="{{asset('assets/vendor/magnific-popup/magnific-popup.js')}}"></script>
	<script src="{{asset('assets/vendor/jquery-placeholder/jquery.placeholder.js')}}"></script>

	<!-- Specific Page Vendor -->
	<script src="{{asset('assets/vendor/select2/select2.js')}}"></script>
	<script src="{{asset('assets/vendor/jquery-datatables/media/js/jquery.dataTables.js')}}"></script>
	<script src="{{asset('assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js')}}"></script>
	<script src="{{asset('assets/vendor/jquery-datatables-bs3/assets/js/datatables.js')}}"></script>
	<script src="cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
	<script src="cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"></script>

	<!-- Theme Base, Components and Settings -->
	<script src="{{asset('assets/javascripts/theme.js')}}"></script>

	<!-- Theme Custom -->
	<script src="{{asset('assets/javascripts/theme.custom.js')}}"></script>

	<!-- Theme Initialization Files -->
	<script src="{{asset('assets/javascripts/theme.init.js')}}"></script>
	<!-- Parsley-->
	<script src="{!! asset('assets/vendor/parsley.js') !!}"></script>
	<script src="{!! asset('sweetAlert/sweetalert2.min.js') !!}"></script>
	<script src="{!! asset('sweetAlert/sweetalert2.all.min.js') !!}"></script> 


	<!-- Examples -->
	<script src="{{asset('assets/javascripts/tables/examples.datatables.default.js')}}"></script>
	<script src="{{asset('assets/javascripts/tables/examples.datatables.row.with.details.js')}}"></script>
	<script src="{{asset('assets/javascripts/tables/examples.datatables.row.with.details1.js')}}"></script>
	<script src="{{asset('assets/javascripts/tables/examples.datatables.row.with.detailsu.js')}}"></script>
	<script src="{{asset('assets/javascripts/tables/examples.datatables.row.with.detailsus.js')}}"></script>
	<script src="{{asset('assets/javascripts/tables/examples.datatables.row.with.detailres.js')}}"></script>
	<script src="{{asset('assets/javascripts/tables/examples.datatables.tabletools.js')}}"></script>

</body>
</html>