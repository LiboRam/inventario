<html lang="es">
@extends('templates.master')

@section('content')

        <header class="panel-heading">
          <h2 class="panel-title">Insertar datos</h2>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
          <meta name="viewport" content="initial-scale=1.0, user-scalable=no">

            <meta charset="utf-8">
            <style type="text/css">
                #map {
                    height: 100%;
                }
                html, body{
                    height: 100%;
                    margin: 0;
                    padding: 0;
                }
            </style>
        </header>
        <div class="panel-body">
              <div class="row">
                <div class="col-sm-12">
                  <div class="mb-md">

                    <a href="{{url ('ubicacions') }}" class="btn btn-primary mt-4 ml-3">Regresar  
                    </a>
                  </div>
                </div>
              </div>

          {!! Form::open(['idUbicacion' => 'dataForm', 'url' => '/ubicacions','data-parsley-validate']) !!}

        <div class="column">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                {!! Form::label('nombreUbicacion', 'Ubicación:'); !!}
                {!! Form::textarea('nombreUbicacion', null, ['placeholder' => 'Ubicacion', 'class' => 'form-control','rows' => '1']); !!}
                </div>
            </div>
        </div>
        
        <div class="column">
            <div class="clearfix">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                    {!! Form::label('calle', 'Calle:'); !!}
                    {!! Form::text('calle', null, ['placeholder' => 'Calle', 'class' => 'form-control', 'id'=>'street_address']); !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="column">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                {!! Form::label('numInterior', 'Número interior:'); !!}
                {!! Form::text('numInterior', null, ['placeholder' => 'N int', 'class' => 'form-control', 'id'=> 'street_number']); !!}
                </div>
            </div>
        </div>

        <div class="column">
            <div class="clearfix">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                    {!! Form::label('numExterior', 'Número exterior:'); !!}
                    {!! Form::text('numExterior', null, ['placeholder' => 'N ext', 'class' => 'form-control' ]); !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="column">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                {!! Form::label('colonia', 'Colonia:'); !!}
                {!! Form::text('colonia', null, ['placeholder' => 'colonia', 'class' => 'form-control']); !!}
                </div>
            </div>
        </div>

        <div class="column">
            <div class="clearfix">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                    {!! Form::label('municipio_id', 'Municipio:'); !!}
                    {!! Form::select('municipio_id', $municipios->pluck('nombreMunicipio', 'idMunicipio'), null, ['class' => 'form-control' ]); !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="column">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                {!! Form::label('lat', 'latitud:'); !!}
                {!! Form::text('lat', null, ['placeholder' => 'lat', 'class' => 'form-control', 'id'=>'lat' ]); !!}
                </div>
            </div>
        </div>

        <div class="column">
            <div class="clearfix">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                    {!! Form::label('lng', 'longitud:'); !!}
                    {!! Form::text('lng', null, ['placeholder' => 'lng', 'class' => 'form-control', 'id'=>'lng' ]); !!}
                    </div>
                </div>
            </div>
        </div>

        <br/>

        <div class="column">
                <div class="form-group">
                      <input id="address" type="textbox" value=" ">
                      <input id="submit" type="button" value="Buscar">
                </div>
        </div>

        <div id="map"></div>

    </div>

        <footer class="panel-footer">
          {!! Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary active')) !!} 
          {!! link_to(URL::previous(), 'Cancelar', ['class' => 'btn btn-default active ',]) !!} 
        </footer>

        <script>

            function initMap() {
               var centro = {lat: 17.05, lng: -96.7167};

                var map = new google.maps.Map(document.getElementById('map'), {
                  zoom: 14,
                  center: centro
                });

                var marker = new google.maps.Marker({
                  position: centro,
                  map: map
                });

            var geocoder = new google.maps.Geocoder();
     
            document.getElementById('submit').addEventListener('click', function() {
                geocodeAddress(geocoder, map);
            });
        }

            function geocodeAddress(geocoder, resultsMap) {
                var address = document.getElementById('address').value;

                geocoder.geocode({'address': address}, function(results, status) {
                  if (status === 'OK') {
                    var marker = new google.maps.Marker({
                      map: resultsMap,
                      zoom: 17,
                      draggable: true,
                      position: results[0].geometry.location
                    });

                    document.getElementById('lat').value = results[0].geometry.location.lat();
                    document.getElementById('lng').value = results[0].geometry.location.lng();
                    document.getElementById('nombreUbicacion').value = results[0].formatted_address;

                    google.maps.event.addListener(marker,'dragend',function(event) {
                        document.getElementById('lat').value = this.getPosition().lat();
                        document.getElementById('lng').value = this.getPosition().lng();

                    geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            var address=results[0]['formatted_address'];
                                document.getElementById('nombreUbicacion').value = address;
                        }
                    });                    
                 });

                  } else {
                    alert('La localizacion no fue satisfactoria por la siguiente razon: ' + status);
                    }
                });
            }
        </script>

        
        <script async defer
            src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&callback=initMap&libraries=places&initAutocomplete">
        </script>    
    
       
        {!! Form::close() !!}
            
@endsection()