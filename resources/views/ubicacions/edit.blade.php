@extends('templates.master')
@section('content')

<header class="panel-heading">
    <h2 class="panel-title">Actualizar datos</h2>
</header>
<div class="panel-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="mb-md">

                <a href="{{url ('ubicacions') }}" class="btn btn-primary mt-4 ml-3" style="margin-bottom: 15px;">Regresar
                </a>
            </div>
        </div>
    </div>

    {!! Form::open(['idUbicacion' => 'dataForm', 'method' => 'PATCH', 'url' => '/ubicacions/' . $ubicacions->idUbicacion ]) !!}

    <div class="colum">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                {!! Form::label('nombreUbicacion', 'Ubicación:'); !!}
                {!! Form::text ('nombreUbicacion', $ubicacions->nombreUbicacion, ['placeholder' => 'Agregar ubicacion', 'class' => 'form-control', 'required', 'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
            </div>
        </div>
    </div>

    <div class="colum">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                {!! Form::label('calle', 'Calle:'); !!}
                {!! Form::text('calle', $ubicacions->calle, ['placeholder' => 'Calle', 'class' => 'form-control', 'required', 'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
            </div>
        </div>
    </div>

    <div class="colum">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                {!! Form::label('numInterior', 'Número interior:'); !!}
                {!! Form::text('numInterior', $ubicacions->numInterior, ['placeholder' => 'Numero interior', 'class' => 'form-control', 'required', 'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
            </div>
        </div>
    </div>

    <div class="colum">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
             <div class="form-group">
                {!! Form::label('numExterior', 'Número exterior:'); !!}
                {!! Form::text('numExterior', $ubicacions->numExterior, ['placeholder' => 'Numero exterior', 'class' => 'form-control', 'required', 'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
            </div>
        </div>
    </div>

    <div class="colum">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
             <div class="form-group">
                {!! Form::label('colonia', 'Colonia:'); !!}
                {!! Form::text('colonia', $ubicacions->colonia, ['placeholder' => 'Colonia', 'class' => 'form-control', 'required', 'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
            </div>
        </div>
    </div>

    <div class="colum">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                {!! Form::label('latitud', 'Latitud:'); !!}
                {!! Form::text('lat', $ubicacions->lat, ['placeholder' => 'latitud', 'class' => 'form-control', 'required', 'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
            </div>
        </div>
    </div>

    <div class="colum">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">            
            <div class="form-group">
                {!! Form::label('longitud', 'Longitud:'); !!}
                {!! Form::text('lng', $ubicacions->lng, ['placeholder' => 'longitud', 'class' => 'form-control', 'required', 'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
            </div>
        </div>
    </div>

    <div class="colum">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                {!! Form::label('municipio_id', 'Municipio:'); !!}
                {!! Form::select('municipio_id', $municipios->pluck('nombreMunicipio', 'idMunicipio'), null, ['class' => 'form-control', 'required', 'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
            </div>
        </div>
    </div>
</div>

    <footer class="panel-footer">
      {!! Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary active')) !!} 
      {!! link_to(URL::previous(), 'Cancelar', ['class' => 'btn btn-default active',]) !!} 
    </footer>
@endsection()

<script>
  $(document).ready(function(){
   $('form').parsley();
});
</script>
