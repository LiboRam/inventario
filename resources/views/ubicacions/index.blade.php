@extends('templates.master')

@section('content')
<!-- start: page -->
<header class="panel-heading">
    <h2 class="panel-title">Listado de ubicacion</h2>
</header>
<div class="panel-body">
  <div class="row">
   <div class="col-lg-12 col-md-10 col-sm-10 col-xs-12">
     <div class="mb-md">
      <a href="{{url ('ubicacions/create') }}" class="btn btn-primary mt-4 ml-3"> <i class="fa fa-plus">Agregar</i>  
      </a>
    </div>
  </div>
</div>

@if(Session::has('message'))
<div class="alert alert-danger alert-dismissible mt-5">
  <strong>{!! Session('message') !!}</strong>
  <button type="button" class="close" data-dismiss="alert">
    <span>x</span>
  </button>
</div>
@endif()

<table class="table table-bordered table-hover table-striped mb-none" id="datatable-detailsu">
  <thead>
    <tr>
      <th class="hide" style="padding-left: 15px;" width="40px;">ID</th>
      <th><?php echo utf8_decode("Ubicación");?></th>
      <th class="hide">Calle</th>
      <th class="hide"><?php echo utf8_decode("Número interior");?></th>
      <th class="hide"><?php echo utf8_decode("Número exterior");?></th>
      <th class="hide">Colonia</th>
      <th class="hide">Municipio</th>
      <th>Latitud</th>
      <th>Longitud</th>
      <th width="80px;">Acciones</th>
    </tr>
  </thead>

  <tbody>
    @foreach($ubicacions as $ub)
    <td class="hide" style="padding-left: 15px;">{{$ub->idUbicacion}}></td>
    <td> <?php echo utf8_decode($ub->nombreUbicacion);?> </td>
    <td class="hide"><?php echo utf8_decode($ub->calle);?></td>
    <td class="hide">{{ $ub->numInterior }}</td>
    <td class="hide">{{ $ub->numExterior }}</td>
    <td class="hide"><?php echo utf8_decode($ub->colonia);?></td>
    <td class="hide"><?php echo utf8_decode($ub->Municipio->nombreMunicipio);?></td>
    <td>{{ $ub->lat }}</td>
    <td>{{ $ub->lng }}</td>
    <td class="actions">
      {!! Form::open(['route'=>['ubicacions.destroy', $ub->idUbicacion],'method'=>'DELETE', 'class' => 'form-horizontal','role' => 'form', 'onsubmit' => 'return ConfirmDelete()']) !!}  
      <a href="ubicacions/{!! $ub->idUbicacion !!}/edit" class="btn btn-default btn-sm" role="button"><i class="fa fa-pencil"></i></a>
      <button type="submit" name="button" class="btn btn-default btn-sm">
        <i class="fa fa-trash-o"></i>
      </button>
      {!! Form::close() !!}
    </td>
  </tr>
  @endforeach
</tbody>
</table>
</div>
<script type="text/javascript">
  function ConfirmDelete()
  {
    var x = confirm("Estas seguro de Eliminar?");
    if (x)
      return true;
    else
      return false;
  } 
</script>
@endsection()