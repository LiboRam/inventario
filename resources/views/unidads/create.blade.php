@extends('templates.master')
@section('content')
<header class="panel-heading">
    <h2 class="panel-title">Insertar datos de la unidad</h2>
</header>
<div class="panel-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="mb-md">

                <a href="{{url ('unidads') }}" class="btn btn-primary mt-4 ml-3">Regresar  
                </a>
            </div>
        </div>
    </div>
    {!! Form::open(['id' => 'dataForm', 'url' => '/unidads','data-parsley-validate']) !!}

    
    <div class="col-sm-8 col-md-offset-2  col-xs-12">
        <div class="form-group">
            {!! Form::label('nombreUnidad', 'Nombre de la unidad:'); !!}
            {!! Form::text('nombreUnidad', null, ['placeholder' => 'unidad de medida' ,'required',
            'data-parsley-required-message' =>'Por favor rellene este campo', 'class' => 'form-control']); !!}
        </div>
    </div>
</div>
<footer class="panel-footer">
  {!! Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary active')) !!} 
  {!! link_to(URL::previous(), 'Cancelar', ['class' => 'btn btn-default active',]) !!} 
</footer>
@endsection()

<script>
  $(document).ready(function(){
   $('form').parsley();
});
</script>