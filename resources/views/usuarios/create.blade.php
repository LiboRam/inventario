@extends('templates.master')
@section('content')

  <header class="panel-heading">
    <h2 class="panel-title">Insertar datos</h2>
  </header>
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-12">
          <div class="mb-md">
            <a href="{{url ('usuarios') }}" class="btn btn-primary" style="margin-bottom: 15px;">Regresar  
            </a>
          </div>
        </div>
      </div>

        {!! Form::open(['idUsuario' => 'dataForm', 'url' => '/usuarios','data-parsley-validate']) !!}

          <div class="column">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
              {!! Form::label('username', 'Nombre de Usuario:'); !!}
              {!! Form::text('username', null, ['placeholder' => 'Nombre', 'class' => 'form-control','required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
              </div>
            </div>
          </div>

        <div class="column">
          <div class="clearfix">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
             {!! Form::label('password', 'contraseña:'); !!}
             {!! Form::password('password', null, ['placeholder' => 'contraseña', 'class' => 'form-control','required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
              </div>
            </div>
          </div>
        </div>

        <div class="column">
         <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <div class="form-group">
            {!! Form::label('nombreUsuario', 'Nombre:'); !!}
            {!! Form::text('nombreUsuario', null, ['placeholder' => 'Nombre', 'class' => 'form-control','required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
          </div>
        </div>
        </div>

        <div class="column">
          <div class="clearfix">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
              {!! Form::label('apellidoP', 'Apellido Paterno:'); !!}
              {!! Form::text('apellidoP', null, ['placeholder' => 'Apellido Paterno', 'class' => 'form-control','required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
              </div>
            </div>
          </div>
        </div>

        <div class="column">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
            {!! Form::label('apellidoM', 'Apellido Materno:'); !!}
            {!! Form::text('apellidoM', null, ['placeholder' => 'Apellido Materno', 'class' => 'form-control','required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
            </div>
          </div>
        </div>

        <div class="column">
          <div class="clearfix">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
              {!! Form::label('puesto', 'Puesto:'); !!}
              {!! Form::text('puesto', null, ['placeholder' => 'Puesto del usuario', 'class' => 'form-control','required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
              </div>
            </div>
          </div>
        </div>

        <div class="column">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
            {!! Form::label('fechaCreacion', 'Fecha de creacion:'); !!}
            {!! Form::input('date','fechaCreacion', date('Y-m-d'), ['placeholder' => 'dd/mm/aa', 'class' => 'form-control','required','data-parsley-required-message' =>'Por favor rellene este campo']); !!}
            </div>
          </div>
        </div>

        <div class="column">
          <div class="clearfix">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
              <div class="form-group">
              {!! Form::label('area_id', 'Area:'); !!}
              {!! Form::select('area_id', $area->pluck('nombreArea', 'idArea'), null, ['class' => 'form-control','placeholder' => '--selecciona--','required','data-parsley-required-message' =>'Seleccione el area']); !!}
              </div>
            </div>
          </div>
        </div>

    </div>

    <footer class="panel-footer">
        {!! Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary active')) !!} 
        {!! link_to(URL::previous(), 'Cancelar', ['class' => 'btn btn-default active',]) !!} 
    </footer>
        @endsection()

  <script>
    $(document).ready(function(){
     $('form').parsley();
   });
  </script>