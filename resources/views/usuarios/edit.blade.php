@extends('templates.master')
@section('content')

<header class="panel-heading">
    <h2 class="panel-title">Actualizar datos</h2>
</header>
<div class="panel-body">
    <div class="row">
        <div class="col-sm-12">
            <div class="mb-md">

                <a href="{{url ('usuarios') }}" class="btn btn-primary mt-4 ml-3">Leer datos 
                </a>
            </div>
        </div>
    </div>
    
    {!! Form::open(['idUsuario' => 'dataForm', 'method' => 'PATCH', 'url' => '/usuarios/' . $usuario->idUsuario ]) !!}

    <div class="column">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                {!! Form::label('username', 'Nombre de usuario:'); !!}
                {!! Form::text('username', $usuario->usename, ['placeholder' => 'Agregar usuario', 'class' => 'form-control', 'required', 'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
            </div>
        </div>
    </div>
    <div class="column">
        <div class="clearfix">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="form-group">
                    {!! Form::label('password', 'contraseña:'); !!}
                    {!! Form::text('password', $usuario->password, ['placeholder' => 'Contraseña', 'class' => 'form-control', 'required', 'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
                </div>
            </div>
        </div>
    </div>
    <div class="column">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                {!! Form::label('nombreUsuario', 'Nombre:'); !!}
                {!! Form::text('nombreUsuario', $usuario->nombreUsuario, ['placeholder' => 'Nombre', 'class' => 'form-control', 'required', 'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
            </div>
        </div>
    </div>
    <div class="column">
        <div class="clearfix">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               <div class="form-group">
                {!! Form::label('apellidoP', 'Apellido Paterno:'); !!}
                {!! Form::text('apellidoP', $usuario->apellidoP, ['placeholder' => 'Apellido Paterno', 'class' => 'form-control', 'required', 'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
            </div>
        </div>
    </div>
</div>
<div class="column">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
       <div class="form-group">
        {!! Form::label('apellidoM', 'Apellido Materno:'); !!}
        {!! Form::text('apellidoM', $usuario->apellidoM, ['placeholder' => 'Apellido Materno', 'class' => 'form-control', 'required', 'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
    </div>
</div>
</div>
<div class="column">
    <div class="clearfix">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                {!! Form::label('puesto', 'puesto:'); !!}
                {!! Form::text('puesto', $usuario->puesto, ['placeholder' => 'puesto', 'class' => 'form-control', 'required', 'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
            </div>
        </div>
    </div>
</div>
<div class="column">
 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="form-group">
        {!! Form::label('fechaCreacion', 'Fecha de creacion:'); !!}
        {!! Form::input('date(Y-m-d)', 'fechaCreacion', $usuario->fechaCreacion, ['class' => 'form-control', 'required', 'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
    </div>
</div>
</div> 
<div class="column">
    <div class="clearfix">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                {!! Form::label('area_id', 'Area:'); !!}
                {!! Form::select('area_id', $area->pluck('nombreArea', 'idArea'), null, ['class' => 'form-control', 'required', 'data-parsley-required-message' =>'Por favor rellene este campo']); !!}
            </div>
        </div>
    </div>
</div>
</div>
<footer class="panel-footer">
  {!! Form::button('Guardar', array('type' => 'submit', 'class' => 'btn btn-primary active')) !!} 
  {!! link_to(URL::previous(), 'Cancelar', ['class' => 'btn btn-default active',]) !!} 
</footer>
@endsection()