@extends('templates.master')
@section('content')

<header class="panel-heading">
  <span class="separator">
    <h2 class="panel-title">Listado de usuarios</h2>
  </span>
</header>
<div class="panel-body">
  <div class="row">
   <div class="col-lg-12 col-md-10 col-sm-10 col-xs-12">
    <div class="mb-md">
      <a href="{{url ('register') }}" class="btn btn-primary mt-4 ml-3" style="margin-bottom: 15px;"> <i class="fa fa-plus">Agregar</i>  
      </a>
    </div>
   </div>
  </div>

   @if(Session::has('message'))
   <div class="alert alert-danger alert-dismissible mt-5">
    <strong>{!! Session('message') !!}</strong>
    <button type="button" class="close" data-dismiss="alert">
      <span>x</span>
    </button>
  </div>
  @endif()
    <table class="table table-bordered table-hover mb-none table-striped" id="datatable-detailsus">
      <thead>
        <tr>
          <th class="hide">Clave</th>
          <th>Usuario</th>
          <th>Nombre</th>
          <th class="hide"><?php echo utf8_decode("Contraseña");?></th>
          <th>Puesto</th>
          <th class="hide">feche de creacion</th>
          <th class="hide">Area</th>
          <th width="80px;">Acciones
          </th>
        </tr>
      </thead>
  <tbody>

    @foreach($usuario as $us)
      <td class="hide">{!! $us->idUsuario !!}</td>
      <td>{!! $us->username !!}
      <td>{!! $us->name_complete !!}</td>
      <td class="hide">{!! $us->password !!}</td>
      <td>{!! $us->puesto !!}</td>
      <td class="hide">{!! $us->fechaCreacion !!}
      <td class="hide">{!! $us->Area->nombreArea !!}</td>
     <td class="actions">
        {!! Form::open(['route'=>['usuarios.destroy', $us->idUsuario],'method'=>'DELETE', 'class' => 'form-horizontal','role' => 'form', 'onsubmit' => 'return ConfirmDelete()']) !!}  
        <a href="usuarios/{!! $us->idUsuario !!}/edit" class="btn btn-default btn-sm" role="button"><i class="fa fa-pencil"></i></a>
        <button type="submit" name="button" class="btn btn-default btn-sm">
          <i class="fa fa-trash-o"></i>
        </button>
        {!! Form::close() !!}
      </td>
    
    @endforeach
  </tbody>
</table>

</div>
<script type="text/javascript">
  function ConfirmDelete()
  {
    var x = confirm("Estas seguro de Eliminar?");
    if (x)
      return true;
    else
      return false;
  } 
</script>

@endsection()