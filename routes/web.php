<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Route::get('/', function() {
	return view('index');
});


Route::get('/images/{file}', function ($file) {
	return Storage::response("images/$file");
})->where([
	'file' => '(.*?)\.(jpg|png|jpeg)$'
]);


//Route::post('comment/{comment}');

Route::resource('marcas', 'MarcaController');

Route::resource('modelos', 'ModeloController');

Route::resource('categorias', 'CategoriaController');

Route::resource('unidads', 'UnidadController');

Route::resource('edoequipos', 'EdoEquiposController');

Route::resource('equipos', 'EquipoController');

Route::resource('solicitantes', 'SolicitanteController');

Route::resource('municipios', 'MunicipioController');

Route::resource('ubicacions', 'UbicacionController');

Route::resource('areas', 'AreaController');

Route::resource('usuarios', 'UsuarioController');
Route::post('addUsuario', 'UsuarioController@addUsuario');

Route::resource('solicituds', 'SolicitudController');

Route::resource('resguardos', 'ResguardoController');

Route::resource('detalle_equipos', 'DetalleEquiposController');

Route::resource('resguardo_equipos', 'ResguardoEquiposController');

Route::resource('detalle_solicituds', 'DetalleSolicitudsController');

Route::post('mensaje/{slug?}/delete', 'MunicipioController@destroy');
Route::post('mensaje/{slug?}/delete', 'AreaController@destroy');


//stock-equipos
Route::get('/stock-equipos', 'EquipoController@stock');

Route::get('/informe-equipos', 'EquipoController@informe');
Route::get('/informe-resguardos', 'ResguardoController@informe');
Route::get('/informe-solicitud', 'SolicitudController@show');


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


//Rutas sobre informes en pdf
Route::name('downloadScte')->get('solicitantes/show', 'SolicitanteController@show');
Route::name('descargarEq')->get('equipos/show', 'EquipoController@show');
Route::name('generateForm')->get('solicituds/generate','SolicitudController@show');


//Route::get('/solicituds/{idSolicitud}/generate', 'SaleController@show');
